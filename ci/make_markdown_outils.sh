#!bin/bash

# Construction des fichiers markdown à partir des notebooks
cd notebooks

## Introduction
jupyter nbconvert --to markdown IntroR.ipynb
cp IntroR.md ../docs/

## Manipulation de données
jupyter nbconvert --to markdown ManipDon.ipynb
cp ManipDon.md ../docs/

## Objets R
jupyter nbconvert --to markdown Objets.ipynb
cp Objets.md ../docs/


## Graphiques
jupyter nbconvert --to markdown GraphiqueR.ipynb
cp GraphiqueR.md ../docs/

# Elements de programmation
jupyter nbconvert --to markdown ProgrammerR.ipynb
cp ProgrammerR.md ../docs/

## Tutoriel ggplot2
cd Rgraphics
jupyter nbconvert --to markdown Rgraphics.ipynb
cp Rgraphics.md ../../docs

# Les études
cd ../../TP/notebooks
# Concentrations moyenne pour l'année 2016 en polluant
jupyter nbconvert --to markdown mesureNO2_2016.ipynb
cp mesureNO2_2016.md ../../docs/
jupyter nbconvert --to markdown mesureNO2_2016_Correct.ipynb
cp mesureNO2_2016_Correct.md ../../docs/
jupyter nbconvert --to markdown ObjetManip.ipynb
cp ObjetManip.md ../../docs
# Traitement des dates
jupyter nbconvert --to markdown TraitementDate.ipynb
cp TraitementDate.md ../../docs/


