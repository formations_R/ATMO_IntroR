
*Ce notebook est issu du document fourni par H. Commenges lors d'une formation pour ATMO en avril 2017*.

Vous le trouverez dans les fichiers fournis sous : **.../ATMO_IntroR/TP/notebooks/ObjetManip.ipynb**

Pour vous exercer:

* **Utiliser le notebbok**: jupyter notebook ObjetManip.ipynb

* **Sous RStudio**, recopier les commandes des cellules R et tester.

## Création, manipulation d'objets R et de structures de données

### Création d'objets

#### Assignation

* Opérateur d'assignation "**<-**"
* "**<-**" et "**=**" sont équivalents

L'opérateur "**<-**" est conseillé


```R
1+1
Var1Somme <- 1+1
Var1Somme
Var2Somme <- 7+6
Var2Somme
VarSommeGlob <- Var1Somme + Var2Somme
VarSommeGlob
# Essayer d'autres opérations 
...
```


2



2



13



15



```R
Var2Somme <- 8
Var2Somme
```


8


#### Référencement
* Si le nom n'existe pas, l'assignation crée de nouvelle référence.
* Si le nom existe, l'assignation écrase la référence existante.

### Typage dynamique

#### Typage à la volée


```R
#
estNum <- 1.67
estInt <- 45L
estAlphaNum <- "taux"
estLogique <- TRUE
```

Se renseigner et changer de type


```R
class(estNum)
mode(estNum)
is.numeric(estNum)
#
class(estInt)
mode(estInt)
is.numeric(estInt)
#
class(estAlphaNum)
is.numeric(estAlphaNum)
```


'numeric'



'numeric'



TRUE



'integer'



'numeric'



TRUE



'character'



FALSE


### Calcul terme à terme

* Les opérations sont effectués vectoriellement terme à terme
* il y a des opérateurs spécifiques pour le calcul matriciel


```R
vec1 <- c(3,8,-8,10,11)
vec1
vec2 <- rep(4,5)
vec2
```


<ol class=list-inline>
	<li>3</li>
	<li>8</li>
	<li>-8</li>
	<li>10</li>
	<li>11</li>
</ol>




<ol class=list-inline>
	<li>4</li>
	<li>4</li>
	<li>4</li>
	<li>4</li>
	<li>4</li>
</ol>




```R
# Opération
vec1 + vec2
vec1*6
vec1*vec2
vec1%*%vec2
```


<ol class=list-inline>
	<li>7</li>
	<li>12</li>
	<li>-4</li>
	<li>14</li>
	<li>15</li>
</ol>




<ol class=list-inline>
	<li>18</li>
	<li>48</li>
	<li>-48</li>
	<li>60</li>
	<li>66</li>
</ol>




<ol class=list-inline>
	<li>12</li>
	<li>32</li>
	<li>-32</li>
	<li>40</li>
	<li>44</li>
</ol>




<table>
<tbody>
	<tr><td>96</td></tr>
</tbody>
</table>



## 

## Manipulation de structure de données

### Les Vecteurs
Suite ordonnée de valeurs **de même type**(numérique, alphanumérique, logique) en *une dimension*

* Créer l'objet


```R
c(6,7,1,2,9)
vecTaille <- c(10, 40, 20, 17, 12)
```


<ol class=list-inline>
	<li>6</li>
	<li>7</li>
	<li>1</li>
	<li>2</li>
	<li>9</li>
</ol>



* Se renseigner sur l'objet


```R
str(vecTaille)
class(vecTaille)
length(vecTaille)
names(vecTaille)
```

     num [1:5] 10 40 20 17 12



'numeric'



5



    NULL


* Désigner des valeurs


```R
vecTaille[3]
vecTaille[c(1,3)]
vecTaille[1:3]
```


20



<ol class=list-inline>
	<li>10</li>
	<li>20</li>
</ol>




<ol class=list-inline>
	<li>10</li>
	<li>40</li>
	<li>20</li>
</ol>



* Utiliser des fonctions


```R
sum(vecTaille[3])
mean(vecTaille)
summary(vecTaille)
```


20



19.8



       Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
       10.0    12.0    17.0    19.8    20.0    40.0 


### Les matrices

* Suite de valeurs de **même type** (numérique, alphanumérique, logique) en **n dimensions**
* *Classe* : **matrix**(2 dimensions) ou **array** (n dimensions)

#### Créer l'objet:


```R
seq(0,6,2)
mat <- matrix(c(c(2.3,4,7.8,9.2),rep(2,4),seq(0,6,2)),nrow=4,ncol=3)
mat
```


<ol class=list-inline>
	<li>0</li>
	<li>2</li>
	<li>4</li>
	<li>6</li>
</ol>




<table>
<tbody>
	<tr><td>2.3</td><td>2  </td><td>0  </td></tr>
	<tr><td>4.0</td><td>2  </td><td>2  </td></tr>
	<tr><td>7.8</td><td>2  </td><td>4  </td></tr>
	<tr><td>9.2</td><td>2  </td><td>6  </td></tr>
</tbody>
</table>



#### Se renseigner sur l'objet



```R
str(mat)
class(mat)
dim(mat)
rownames(mat)
colnames(mat)
```

     num [1:4, 1:3] 2.3 4 7.8 9.2 2 2 2 2 0 2 ...



'matrix'



<ol class=list-inline>
	<li>4</li>
	<li>3</li>
</ol>




    NULL



    NULL


#### Désigner les valeurs
Les valeurs sont stockées colonne par colonne, on peut modifier en spécifiant l'option **byrow=TRUE**


```R
mat[4]
mat[1,2]
mat[1,] # ligne 1
mat[,1] # colonne 1
matrow <- matrix(c(c(2.3,4,7.8,9.2),rep(2,4),seq(0,6,2)),nrow=4,ncol=3,byrow=TRUE)
matrow
matrow[4]
```


9.2



2



<ol class=list-inline>
	<li>2.3</li>
	<li>2</li>
	<li>0</li>
</ol>




<ol class=list-inline>
	<li>2.3</li>
	<li>4</li>
	<li>7.8</li>
	<li>9.2</li>
</ol>




<table>
<tbody>
	<tr><td>2.3</td><td>4  </td><td>7.8</td></tr>
	<tr><td>9.2</td><td>2  </td><td>2.0</td></tr>
	<tr><td>2.0</td><td>2  </td><td>0.0</td></tr>
	<tr><td>2.0</td><td>4  </td><td>6.0</td></tr>
</tbody>
</table>




2


#### Faire des opérations


```R
sum(mat)
mean(mat)
mean(mat[,1])
summary(mat)
```


43.3



3.60833333333333



5.825



           V1              V2          V3     
     Min.   :2.300   Min.   :2   Min.   :0.0  
     1st Qu.:3.575   1st Qu.:2   1st Qu.:1.5  
     Median :5.900   Median :2   Median :3.0  
     Mean   :5.825   Mean   :2   Mean   :3.0  
     3rd Qu.:8.150   3rd Qu.:2   3rd Qu.:4.5  
     Max.   :9.200   Max.   :2   Max.   :6.0  


### Tableau

* Ensemble de **vecteurs** de **même taille** mais pouvant être **de type différent**.

* Classe : **data.frame**

#### Créer l'objet


```R
tabCastors1 <- data.frame(NOM=c("Riri","Fifi","Loulou"),
                        TAILLE=c(1.43,1.78,1.03),
                        POIDS=c(50,87,45))
```


```R
tabCastors1
# Nom des lignes
row.names(tabCastors1)
```


<table>
<thead><tr><th scope=col>NOM</th><th scope=col>TAILLE</th><th scope=col>POIDS</th></tr></thead>
<tbody>
	<tr><td>Riri  </td><td>1.43  </td><td>50    </td></tr>
	<tr><td>Fifi  </td><td>1.78  </td><td>87    </td></tr>
	<tr><td>Loulou</td><td>1.03  </td><td>45    </td></tr>
</tbody>
</table>




<ol class=list-inline>
	<li>'1'</li>
	<li>'2'</li>
	<li>'3'</li>
</ol>




```R
tabCastors2 <- data.frame(NOM=c("Riri","Fifi","Loulou"),
                        TAILLE=c(1.43,1.78,1.03),
                        POIDS=c(50,87,45),row.names="NOM")
row.names(tabCastors2)
```


<ol class=list-inline>
	<li>'Riri'</li>
	<li>'Fifi'</li>
	<li>'Loulou'</li>
</ol>



#### Se renseigner sur l'objet


```R
str(tabCastors2)
class(tabCastors2)
dim(tabCastors2)
row.names(tabCastors2)
colnames(tabCastors2)
```

    'data.frame':	3 obs. of  2 variables:
     $ TAILLE: num  1.43 1.78 1.03
     $ POIDS : num  50 87 45



'data.frame'



<ol class=list-inline>
	<li>3</li>
	<li>2</li>
</ol>




<ol class=list-inline>
	<li>'Riri'</li>
	<li>'Fifi'</li>
	<li>'Loulou'</li>
</ol>




<ol class=list-inline>
	<li>'TAILLE'</li>
	<li>'POIDS'</li>
</ol>



#### Faire des opérations


```R
summary(tabCastors1)
summary(tabCastors2)
```


         NOM        TAILLE          POIDS      
     Fifi  :1   Min.   :1.030   Min.   :45.00  
     Loulou:1   1st Qu.:1.230   1st Qu.:47.50  
     Riri  :1   Median :1.430   Median :50.00  
                Mean   :1.413   Mean   :60.67  
                3rd Qu.:1.605   3rd Qu.:68.50  
                Max.   :1.780   Max.   :87.00  



         TAILLE          POIDS      
     Min.   :1.030   Min.   :45.00  
     1st Qu.:1.230   1st Qu.:47.50  
     Median :1.430   Median :50.00  
     Mean   :1.413   Mean   :60.67  
     3rd Qu.:1.605   3rd Qu.:68.50  
     Max.   :1.780   Max.   :87.00  



```R
# sum(tabCastors1)
sum(tabCastors2)
sum(tabCastors1$TAILLE)
mean(tabCastors2$TAILLE)
summary(tabCastors2)
summary(tabCastors2$TAILLE)
attributes(tabCastors)
```


186.24



4.24



1.41333333333333



         TAILLE          POIDS      
     Min.   :1.030   Min.   :45.00  
     1st Qu.:1.230   1st Qu.:47.50  
     Median :1.430   Median :50.00  
     Mean   :1.413   Mean   :60.67  
     3rd Qu.:1.605   3rd Qu.:68.50  
     Max.   :1.780   Max.   :87.00  



       Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
      1.030   1.230   1.430   1.413   1.605   1.780 



    Error in eval(expr, envir, enclos): object 'tabCastors' not found
    Traceback:



### Liste

* Ensemble d'objets **de tout type** et **de toute taille**

* Classe : **list**

#### Créer un objet


```R
riri <- list(NOM="Riri",TAILLE=data.frame(DATE=c(1951,1952,1953),TAILLE=c(1.43,1.48,1.54)),
                                         COORDS=c(33.94,-117.51))
fifi <- list(NOM="Fifi",TAILLE=data.frame(DATE=c(1951,1952,1953),TAILLE=c(1.78,1.80,1.87)),
                                         COORDS=c(33.21,-117.64))
loulou <- list(NOM="Loulou",TAILLE=data.frame(DATE=c(1951,1952,1953),TAILLE=c(1.04,1.10,1.20)),
                                         COORDS=c(33.83,-117.25))
listCastors <-list(RIRI=riri,FIFI=fifi,LOULOU=loulou)
```

#### Se renseigner sur l'objet


```R
str(riri)
class(riri)
str(listCastors)
class(listCastors)
length(listCastors)
names(riri)
names(listCastors)
```

#### Désigner des valeurs


```R
riri[1]
class(riri[1])
riri[[1]]
class(riri[[1]])
riri$NOM
```


```R
#
listCastors[[1]][[2]]
class(listCastors[[1]][[2]])
listCastors[[1]][2]
class(listCastors[[1]][2])
```

#### Faire des opérations

Il faut appliquer des fonctions sur un ensemble d'éléments de la liste (**lapply**,...)

## Manipulation simple

### Indexation et sélection

* L'indexation consiste à désigner par un numéro ou une liste de numéro (vecteur,...) une ou plusieurs valeurs d'un vecteur, d'une matrice, d'un tableau.
* L'indexation peut aussi se faire avec un vecteur logique de même taille.


```R
# Cas d'un vecteur
vecTaille
vecTaille > 15
testBool <- vecTaille > 15
vecTaille[testBool]
# Ce qui peut aussi s'ecrire directement
vecTaille[vecTaille > 15]
```


```R
# Cas d'une matrice
mat > 2
testBoolMat <- mat > 2
mat[testBoolMat]
# Ce qui peut aussi s'ecrire directement
mat[mat > 2]
mat[mat > 2] <- NA
mat
mean(mat)
mean(mat,na.rm=TRUE)
```


```R
# Cas d'un tableau
dim(tabCastors2)
tabCastors2$TAILLE >1.5
testBoolCastors <- tabCastors2$TAILLE >1.5
tabCastors2[testBoolCastors,]
# Ce qui peut aussi s'ecrire directement
tabCastors2[tabCastors2$TAILLE >1.5,]
```

### Assignation au sein d'un tableau

Au sein d'un tableau ou d'une liste, les composantes ou variables sont désignées par des noms. Le principe d'assignation s'applique à ces composantes.

* Si j'assigne à un nom de variable qui n'existe pas dans le tableau, une nouvelle variable est créée avec ce nom.<br>


* Si j'assigne à un nom de variable qui existe déjà dans le tableau, la variable est écrasée par cette nouvelle assignation


```R
# La composante n'esiste pas
tabCastors2$IMC <- tabCastors2$POIDS / tabCastors2$TAILLE
tabCastors2$IMC
# La composante existe
tabCastors2$IMC <- 999
tabCastors2$IMC
```
