
# Les objets R

R utilise des fonctions et des opérateurs qui agissent sur des objets, ceux-ci sont caractérisés par leur *nom* et leur *contenu*, mais aussi par **des attributs**. Ce sont les attributs d'un objet qui vont déterminer le comportement des fonctions sur cet objet (méthode générique).

Des méthodes génériques sont prédéfinies et peuvent être redéfinies pour des groupes de fonctions: Math, Math2, Ops, Summary et Complexe.

Pour connaître l'ensemble des méthodes génériques fourni par l'installation de base de R:


```R
help('Math')
```

Un objet a *deux attributs intrinsèques*, son **mode** ou **type** et sa **longueur**.  Il peut avoir des attributs spécifiques qui diffèrent selon le type de l'objet (dim,dimnames,class,...).

* Pour connaître le mode ou type d'un objet **x** de R, il suffit d'exécuter la fonction **mode(x)**

* La fonction **typeof(x)** permet d'obtenir une description plus précise de la représentation interne d'un objet.

* La fonction **length(x)** permet d'obtenir sa longueur.

* La fonction **class(x)** fournit la classe de l'objet x.


```R
# Variables of differents types
my_numeric <- 42
my_character <- "42"
my_logical <- FALSE 
my_integer <- 5L
```


```R
# Class de my_numeric
" mode et classe my_numeric"
mode(my_numeric)
typeof(my_numeric)
class(my_numeric)
```


' mode et classe my_numeric'



'numeric'



'double'



'numeric'



```R
# Class de my_numeric
" mode et classe my_integer"
mode(my_integer)
typeof(my_integer)
class(my_integer)
```


' mode et classe my_integer'



'numeric'



'integer'



'integer'



```R
xx <- my_numeric + my_integer
xx
mode(xx)
class(xx)
```


47



'numeric'



'numeric'



```R
# Class de my_character
" mode et classe my_character"
mode(my_character)
typeof(my_character)
class(my_character)
```


' mode et classe my_character'



'character'



'character'



'character'



```R
# my_character + 10  # argument non numérique pour un opérateur binaire
paste(my_character, "euros") # Concatène après conversion en caractères
b <- paste(my_character, 14)
b
mode(b)
class(b)
```


'42 euros'



'42 14'



'character'



'character'



```R
# Class de my_logical
" mode et classe my_logical"
mode(my_logical)
class(my_logical)
```


' mode et classe my_logical'



'logical'



'logical'



```R
# Longueur et classe d'un vecteur construit avec ":"
vec1 <- 1:4
class(vec1)
length(vec1)
```


'integer'



4



```R
# Longueur et classe d'un vecteur construit avec la fonction "c()"
vec2 <- c(1,2,3,4,6)
class(vec2)
length(vec2)
```


'numeric'



5



```R
# Longueur et classe d'un vecteur vide
vec3 <- c()
class(vec3)
length(vec3)
```


'NULL'



0


## Les types de base dans R

### Les principaux modes ou types d'objet dans R sont: 

   * Un objet vide noté: NULL
   * Les *valeurs réelles* comme **4.5** sont de type **numériques**.
   * Les *nombres naturels* comme **4L** sont appelés *integer*, ils sont de type **numériques**.
   * Les *valeurs booléennes* (TRUE ou FALSE) sont de type **logiques**.
   * Les *chaînes de caractères* comme **"nom"** sont de type **character**.
   * Les * nombres complexes*  comme **2+4i** sont de type **complex**.
 
Les guillemets de part et d'autre d'un texte ( "texte") indique que c'est du type **character**

### Attributs d'un objet - méthode générique

* La **class** et les **attributs d'un objet** déterminent le comportement d'**une méthode générique** sur cet objet.

* La fonction **attributes(x)** fournit les attributs spécifiques de l'objet **x**.

* L'utilisateur qui crée ses propres objets pourra ou devra reécrire les méthodes génériques adaptées à cet objet.

Pour comprendre l'utilité des attributs d'un objet, considérons une variable *age* qui prendrait les valeurs entières de 1 à 10 : une telle variable peut représenter **une variable entière quantitative** d'un groupe d'enfants de 1 à 10 ans, ou le codage d'une variable catégorielle, des classes d'ages d'une population d'individus de 20 à 80 ans.

Le traitement statistique de cette variable ne sera pas le même dans les deux cas : avec R, les attributs de l'objet donnent l'information nécessaire à la définition du traitement d'un objet. *Plus généralement, l'action d'une fonction sur un objet va dépendre des attributs de celui-ci*.


```R
# Variable quantitative
age1 <- c(1:10)
mode(age1)
class(age1)
attributes(age1)
summary(age1)
print(age1)
```


'numeric'



'integer'



    NULL



       Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
       1.00    3.25    5.50    5.50    7.75   10.00 


     [1]  1  2  3  4  5  6  7  8  9 10



```R
# Variable catégorielle
age2 <- as.factor(age1)
mode(age2)
class(age2)
attributes(age2)
summary(age2)
print(age2)
```


'numeric'



'factor'



<dl>
	<dt>$levels</dt>
		<dd><ol class=list-inline>
	<li>'1'</li>
	<li>'2'</li>
	<li>'3'</li>
	<li>'4'</li>
	<li>'5'</li>
	<li>'6'</li>
	<li>'7'</li>
	<li>'8'</li>
	<li>'9'</li>
	<li>'10'</li>
</ol>
</dd>
	<dt>$class</dt>
		<dd>'factor'</dd>
</dl>




<dl class=dl-horizontal>
	<dt>1</dt>
		<dd>1</dd>
	<dt>2</dt>
		<dd>1</dd>
	<dt>3</dt>
		<dd>1</dd>
	<dt>4</dt>
		<dd>1</dd>
	<dt>5</dt>
		<dd>1</dd>
	<dt>6</dt>
		<dd>1</dd>
	<dt>7</dt>
		<dd>1</dd>
	<dt>8</dt>
		<dd>1</dd>
	<dt>9</dt>
		<dd>1</dd>
	<dt>10</dt>
		<dd>1</dd>
</dl>



     [1] 1  2  3  4  5  6  7  8  9  10
    Levels: 1 2 3 4 5 6 7 8 9 10



```R
# fonction generique - objet de classe "matrix"
a <- 1:20
cmat1 <-matrix(a,ncol=5)
class(cmat1)
attributes(cmat1)
print(cmat1)

# L'impression depend des attributs 'class' et 'dim'
cmat2 <-matrix(a,ncol=4)
class(cmat2)
attributes(cmat2)
print(cmat2)
```


'matrix'



<strong>$dim</strong> = <ol class=list-inline>
	<li>4</li>
	<li>5</li>
</ol>



         [,1] [,2] [,3] [,4] [,5]
    [1,]    1    5    9   13   17
    [2,]    2    6   10   14   18
    [3,]    3    7   11   15   19
    [4,]    4    8   12   16   20



'matrix'



<strong>$dim</strong> = <ol class=list-inline>
	<li>5</li>
	<li>4</li>
</ol>



         [,1] [,2] [,3] [,4]
    [1,]    1    6   11   16
    [2,]    2    7   12   17
    [3,]    3    8   13   18
    [4,]    4    9   14   19
    [5,]    5   10   15   20


### Tester/ concertir le mode d'un objet

* Il est possible de tester le **mode** d'un objet **x** de R avec les fonctions:
   - **is.null**(x)
   - **is.numeric**(x)
   - **is.logical**(x)
   - **is.character**(x)
   - **is.complex**(x)
   
Le résultat de ces fonctions est un booléen qui prend les valeurs **TRUE** et **FALSE**

* Il est possible de convertir un objet d'un mode à un autre avec les fonctions:
   - **as.numeric**(x)
   - **as.logical**(x)
   - **as.character**(x)
   - **as.complex**(x)
   
Il faut rester prudent quant à la signification de ces conversions, R retourne toujours un résultat même si la conversion n'a pas de sens...


```R
# Conversion "logical" en "numeric"
a <- TRUE
mode(a)
as.numeric(a)
```


'logical'



1



```R
# Conversion "logical" en "character"
as.character(a)
```


'TRUE'



```R
# Conversion "character" en "numeric"
b <- "3"
as.numeric(b)
6 + as.numeric(b)
6+b
```


3



9



    Error in 6 + b: non-numeric argument to binary operator
    Traceback:



### Valeur manquante et valeurs spéciales

*  Certaines données sont absentes ou inexploitables, elles ne sont pas disponibles pour le traitement, R les note NA pour Not Available.

Pour savoir où se trouvent les données manquantes, on utilise la fonction **is.na(x)** qui retourne un booléen de même longueur que x.

* On peut mentionner également les valeurs spéciales **Inf** pour l'infini et **NaN** pour Not a Number.

  Les fonctions de test associés:

   - **is.finite**(x)
   - **is.infinite**(x)
   - **is.nan**(x)



```R
# Sans donnees manquantes
v <- c(2,6,4,8,2,9,10)
v
mean(v)
summary(v)
```


<ol class=list-inline>
	<li>2</li>
	<li>6</li>
	<li>4</li>
	<li>8</li>
	<li>2</li>
	<li>9</li>
	<li>10</li>
</ol>




5.85714285714286



       Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
      2.000   3.000   6.000   5.857   8.500  10.000 



```R
# Avec donnees manquantes
vv <- c(2,6,4,8,2,NA,10)
mean(vv)
mean(vv,na.rm=TRUE)
summary(vv)
```


    [1] NA



5.33333333333333



       Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
      2.000   2.500   5.000   5.333   7.500  10.000       1 



```R
# Valeurs spéciales
exp(1e10)
```


Inf



```R
w <- c(log(-4),6,9)
w
mean(w)
mean(w,na.rm=TRUE)
```


Inf


    Warning message in log(-4):
    “NaNs produced”


<ol class=list-inline>
	<li>NaN</li>
	<li>6</li>
	<li>9</li>
</ol>




NaN



7.5


### Principaux objets prédéfinis dans R
 
 On distinguera les **objets atomiques**, d'un type unique et les **objets hétérogènes** composés de plusieurs composantes de mode et de longueurs différents.
 
* **Vecteur**: objet atomique composé d'ensemble de valeurs appelées composantes, coordonnées ou éléments (tableau 1D).

* **Matrice**: objet atomique, chaque valeur de la matrice peut être repérée par son numéro de ligne et son numéro de collone (tableau 2D). On peut construire des tableaux de dimension supérieure à 2, avec la fonction **array**. La manipulation de ces tableaux est identique à celle des matrices.

* **Facteur**: vecteurs permettant de traiter les données qualitatives.

* **Listes**: objet hétérogène.  C'est un ensemble ordonné d'objets ou composantes qui n'ont pas tous le même mode et la même longueur.

* **Data-frame**: listes particulières dont les composantes sont de même longueur, mais elles peuvent être de mode différent. Les tableaux de données utilisés en statistiques sont souvent considérés comme des data-frames.

* **Fonction**: une fonction est un objet R (prédéfinie, utilisateur).


## Les vecteurs

### Les attributs

Les vecteurs sont des **objets atomiques** avec deux attributs intrinséques le **mode** (mode()) et sa **longueur** (length()).


```R
# Vecteur numerique
v1 <- c(3,6,7)
v1
mode(v)
length(v)
```


<ol class=list-inline>
	<li>3</li>
	<li>6</li>
	<li>7</li>
</ol>




'numeric'



7



```R
# Vecteur de caractères
v2 <- c("a","b","c","d")
v2
mode(v2)
length(v2)
```


<ol class=list-inline>
	<li>'a'</li>
	<li>'b'</li>
	<li>'c'</li>
	<li>'d'</li>
</ol>




'character'



4


### Les constructeurs de vecteurs

Plusieurs constructeurs sont fournis par R.

#### Les vecteurs numériques 

 * Construction par la **fonction collecteur** "**c**":


```R
 x <- c(1,0,76) 
x
 x <- c(2,6,-1,c(3,4)) 
x
 y <- c(3,0,x)
y
class(y)
```


<ol class=list-inline>
	<li>1</li>
	<li>0</li>
	<li>76</li>
</ol>




<ol class=list-inline>
	<li>2</li>
	<li>6</li>
	<li>-1</li>
	<li>3</li>
	<li>4</li>
</ol>




<ol class=list-inline>
	<li>3</li>
	<li>0</li>
	<li>2</li>
	<li>6</li>
	<li>-1</li>
	<li>3</li>
	<li>4</li>
</ol>




'numeric'


* Construction par l'**opérateur séquence** "**:**" : 


```R
z <- 1:6
class(z)
```


'integer'


* Construction par la **fonction séquence** "**seq**" :


```R
seq(1,6,by=0.5) 
seq(1,6,length=5)
```


<ol class=list-inline>
	<li>1</li>
	<li>1.5</li>
	<li>2</li>
	<li>2.5</li>
	<li>3</li>
	<li>3.5</li>
	<li>4</li>
	<li>4.5</li>
	<li>5</li>
	<li>5.5</li>
	<li>6</li>
</ol>




<ol class=list-inline>
	<li>1</li>
	<li>2.25</li>
	<li>3.5</li>
	<li>4.75</li>
	<li>6</li>
</ol>



* Construction par la **fonction** "**rep**":


```R
rep(1,4) 
rep(c(1,2),each=3); rep(c(1,2),3)
```


<ol class=list-inline>
	<li>1</li>
	<li>1</li>
	<li>1</li>
	<li>1</li>
</ol>




<ol class=list-inline>
	<li>1</li>
	<li>1</li>
	<li>1</li>
	<li>2</li>
	<li>2</li>
	<li>2</li>
</ol>




<ol class=list-inline>
	<li>1</li>
	<li>2</li>
	<li>1</li>
	<li>2</li>
	<li>1</li>
	<li>2</li>
</ol>



* Construction par la **fonction** "**scan**": R demande l'entrée des données au fur et à
mesure. Si n est absent, la fin des entrées est provoquée par une valeur vide.

mesures <- scan(n=4)

#### Les vecteurs de caractères
Il est possible de créer des vecteurs de caractères en utilisant les fonctions "**c**" et "**rep**".


```R
x <- c("A","L","I","C","E") 
x
y <- rep("A",4)
y
```


<ol class=list-inline>
	<li>'A'</li>
	<li>'L'</li>
	<li>'I'</li>
	<li>'C'</li>
	<li>'E'</li>
</ol>




<ol class=list-inline>
	<li>'A'</li>
	<li>'A'</li>
	<li>'A'</li>
	<li>'A'</li>
</ol>



#### Les vecteurs logiques

Les **vecteurs booléens** peuvent être générés par des opérateurs logiques.


```R
n <- 10
a <- c(1>0,n > 15)
a 
```


<ol class=list-inline>
	<li>TRUE</li>
	<li>FALSE</li>
</ol>



#### Les fonctions any et all

* **any**("condition"): TRUE si une composante vérifie la condition
* **all**("condition): TRUE si toutes les composantes verifient la condition


```R
#
x <- c(-1,2,3,-7,5,0)
test <- x > 0
test
#
y <-(1+x^2)*(x>0) # TRUE : 1 ; FALSE : 0
y
# 
all(x>0) # TRUE si tous >0
# 
any(x>0) # TRUE siun des elements > 0
```


<ol class=list-inline>
	<li>FALSE</li>
	<li>TRUE</li>
	<li>TRUE</li>
	<li>FALSE</li>
	<li>TRUE</li>
	<li>FALSE</li>
</ol>




<ol class=list-inline>
	<li>0</li>
	<li>5</li>
	<li>10</li>
	<li>0</li>
	<li>26</li>
	<li>0</li>
</ol>




FALSE



TRUE


### Manipulation de vecteurs

#### Sélection d'une partie d'un vecteur: 

Elle s'opère avec l'opérateur de sélection "**[]**" et un **vecteur de sélection** qui peut être un vecteurs d'entiers
positifs, négatifs ou de logiques:


```R
v <- 1:100
v[6] # 6 ieme element
v[6:15] # les éléments de 6 à 15
v[c(3,3,1,5)] # 3 ieme, 3ieme et premier et cinquieme element
v[4:2] # 4 ieme, 3ieme, 2ieme
v[-(1:5)] # v sans ses 5 premiers elements
v[(v<5)]
v[v<15 & v>=4] # & signifie "et"
```


6



<ol class=list-inline>
	<li>6</li>
	<li>7</li>
	<li>8</li>
	<li>9</li>
	<li>10</li>
	<li>11</li>
	<li>12</li>
	<li>13</li>
	<li>14</li>
	<li>15</li>
</ol>




<ol class=list-inline>
	<li>3</li>
	<li>3</li>
	<li>1</li>
	<li>5</li>
</ol>




<ol class=list-inline>
	<li>4</li>
	<li>3</li>
	<li>2</li>
</ol>




<ol class=list-inline>
	<li>6</li>
	<li>7</li>
	<li>8</li>
	<li>9</li>
	<li>10</li>
	<li>11</li>
	<li>12</li>
	<li>13</li>
	<li>14</li>
	<li>15</li>
	<li>16</li>
	<li>17</li>
	<li>18</li>
	<li>19</li>
	<li>20</li>
	<li>21</li>
	<li>22</li>
	<li>23</li>
	<li>24</li>
	<li>25</li>
	<li>26</li>
	<li>27</li>
	<li>28</li>
	<li>29</li>
	<li>30</li>
	<li>31</li>
	<li>32</li>
	<li>33</li>
	<li>34</li>
	<li>35</li>
	<li>36</li>
	<li>37</li>
	<li>38</li>
	<li>39</li>
	<li>40</li>
	<li>41</li>
	<li>42</li>
	<li>43</li>
	<li>44</li>
	<li>45</li>
	<li>46</li>
	<li>47</li>
	<li>48</li>
	<li>49</li>
	<li>50</li>
	<li>51</li>
	<li>52</li>
	<li>53</li>
	<li>54</li>
	<li>55</li>
	<li>56</li>
	<li>57</li>
	<li>58</li>
	<li>59</li>
	<li>60</li>
	<li>61</li>
	<li>62</li>
	<li>63</li>
	<li>64</li>
	<li>65</li>
	<li>66</li>
	<li>67</li>
	<li>68</li>
	<li>69</li>
	<li>70</li>
	<li>71</li>
	<li>72</li>
	<li>73</li>
	<li>74</li>
	<li>75</li>
	<li>76</li>
	<li>77</li>
	<li>78</li>
	<li>79</li>
	<li>80</li>
	<li>81</li>
	<li>82</li>
	<li>83</li>
	<li>84</li>
	<li>85</li>
	<li>86</li>
	<li>87</li>
	<li>88</li>
	<li>89</li>
	<li>90</li>
	<li>91</li>
	<li>92</li>
	<li>93</li>
	<li>94</li>
	<li>95</li>
	<li>96</li>
	<li>97</li>
	<li>98</li>
	<li>99</li>
	<li>100</li>
</ol>




<ol class=list-inline>
	<li>1</li>
	<li>2</li>
	<li>3</li>
	<li>4</li>
</ol>




<ol class=list-inline>
	<li>4</li>
	<li>5</li>
	<li>6</li>
	<li>7</li>
	<li>8</li>
	<li>9</li>
	<li>10</li>
	<li>11</li>
	<li>12</li>
	<li>13</li>
	<li>14</li>
</ol>




```R
U <- c(FALSE,TRUE,FALSE,FALSE)
T <- c(23,28,21,32) ; xx <- c(45,67,32,1)
T[U]
xx[T>24]
```


28



<ol class=list-inline>
	<li>67</li>
	<li>1</li>
</ol>



* Utilisation des fonctions **is.xxx()** et **which**


```R
# Quelques selections
x <- c(5,-6,4,NA,12,8,NA)
x
x[is.na(x)] <- 0 # les elements NA de x, reçoivent la valeur 0
x
```


<ol class=list-inline>
	<li>5</li>
	<li>-6</li>
	<li>4</li>
	<li>NA</li>
	<li>12</li>
	<li>8</li>
	<li>NA</li>
</ol>




<ol class=list-inline>
	<li>5</li>
	<li>-6</li>
	<li>4</li>
	<li>0</li>
	<li>12</li>
	<li>8</li>
	<li>0</li>
</ol>




```R
y <- c(-4,5,8,2,-1,-10,12)
y
y[y<0] <- -y[y<0] #remplacement des elements negatifs par leur valeur absolue
y
```


<ol class=list-inline>
	<li>-4</li>
	<li>5</li>
	<li>8</li>
	<li>2</li>
	<li>-1</li>
	<li>-10</li>
	<li>12</li>
</ol>




<ol class=list-inline>
	<li>4</li>
	<li>5</li>
	<li>8</li>
	<li>2</li>
	<li>1</li>
	<li>10</li>
	<li>12</li>
</ol>



La fonction **which** donne les indices associés à la valeur TRUE d'un objet logique (vecteur, array,...). 


```R
x <- c(10:15,45:50,1:4)
which.min(x ) 
x[which.min(x )]
which(x==min(x)) 
(1:length(x))[x == min(x)]
```


13



1



13



13


#### Manipulation vecteur de caractères

* Pour la **concaténation** on utilise la fonction **paste()**:


```R
help(paste)
```


```R
paste(1:5)  # identique 
as.character(1:5) # identique 
paste("A",1:5,sep="_")
paste(c("X","Y"),1:4,"txt",sep=".")
paste(c("X","Y"),1:4,sep=".",collapse = "+")
```


<ol class=list-inline>
	<li>'1'</li>
	<li>'2'</li>
	<li>'3'</li>
	<li>'4'</li>
	<li>'5'</li>
</ol>




<ol class=list-inline>
	<li>'1'</li>
	<li>'2'</li>
	<li>'3'</li>
	<li>'4'</li>
	<li>'5'</li>
</ol>




<ol class=list-inline>
	<li>'A_1'</li>
	<li>'A_2'</li>
	<li>'A_3'</li>
	<li>'A_4'</li>
	<li>'A_5'</li>
</ol>




<ol class=list-inline>
	<li>'X.1.txt'</li>
	<li>'Y.2.txt'</li>
	<li>'X.3.txt'</li>
	<li>'Y.4.txt'</li>
</ol>




'X.1+Y.2+X.3+Y.4'


* Pour l'**extraction**, on utilise la fonction **substr()**


```R
substr("formater",4,8)
```


'mater'


## Matrices

Les matrices sont des objets atomiques de dimension 2 de classe "**matrix**"

### Les attributs

Les attributs d'un objet "matrix" sont le **mode**, la **longueur** (length()), un attribut "**dim**" qui fournit le nombre de lignes et de colonnes et un attribut *optionnel* "**dimnames**", une liste qui contient le nom des lignes et des colonnes.
### Constructeurs

#### Un constructeur utilisé pour construire un objet de type "**matrix**" est la fonction "**matrix()**". 

* Par défaut, R range les valeurs en mémoire par colonne.


```R
m1 <- matrix(c(3,4,5,10,23,6,0,-1),ncol=4)
mode(m1)
length(m1)
class(m1)
attributes(m1)
dim(m1)
```


'numeric'



8



'matrix'



<strong>$dim</strong> = <ol class=list-inline>
	<li>2</li>
	<li>4</li>
</ol>




<ol class=list-inline>
	<li>2</li>
	<li>4</li>
</ol>




```R
# Le nombre de lignes est un diviseur du nombre d'elements
m2 <- matrix(1:8,nrow=4)
dim(m2)
m2
```


<ol class=list-inline>
	<li>4</li>
	<li>2</li>
</ol>




<table>
<tbody>
	<tr><td>1</td><td>5</td></tr>
	<tr><td>2</td><td>6</td></tr>
	<tr><td>3</td><td>7</td></tr>
	<tr><td>4</td><td>8</td></tr>
</tbody>
</table>




```R
# Le nombre de lignes n'est pas un diviseur du nombre d'elements
m3 <- matrix(1:8,nrow=3)
dim(m3)
m3
```

    Warning message in matrix(1:8, nrow = 3):
    “data length [8] is not a sub-multiple or multiple of the number of rows [3]”


<ol class=list-inline>
	<li>3</li>
	<li>3</li>
</ol>




<table>
<tbody>
	<tr><td>1</td><td>4</td><td>7</td></tr>
	<tr><td>2</td><td>5</td><td>8</td></tr>
	<tr><td>3</td><td>6</td><td>1</td></tr>
</tbody>
</table>



* Pour les ranger par lignes en mémoire, on utilise l'argument "**byrow**":


```R
m <- matrix(1:8,nrow=2,byrow=TRUE) # stockage par ligne
dim(m)
m
```


<ol class=list-inline>
	<li>2</li>
	<li>4</li>
</ol>




<table>
<tbody>
	<tr><td>1</td><td>2</td><td>3</td><td>4</td></tr>
	<tr><td>5</td><td>6</td><td>7</td><td>8</td></tr>
</tbody>
</table>



* Si la longueur du vecteur est différente du nombre d'éléments de la matrice, R remplit la matrice et indique un warning:

#### Utilisation des fonctions rbind et cbind

**rbind** et **cbind**: Prend une suite d'arguments vectoriels, matriciels ou "data-frame" et les combine par colonnes ou par lignes respectivement. 

Ce sont des fonctions génériques avec des méthodes implémentées pour d'autres classes R.


```R
mr <- rbind(c(1, 2, 3), c(4, 5, 6)) # chaque vecteur une ligne
mr
```


<table>
<tbody>
	<tr><td>1</td><td>2</td><td>3</td></tr>
	<tr><td>4</td><td>5</td><td>6</td></tr>
</tbody>
</table>




```R
mc <- cbind(c(1, 2, 3), c(4, 5, 6)) # chaque vecteur une colonne
mc
```


<table>
<tbody>
	<tr><td>1</td><td>4</td></tr>
	<tr><td>2</td><td>5</td></tr>
	<tr><td>3</td><td>6</td></tr>
</tbody>
</table>



### Sélection d'éléments ou d'une partie de matrice

* Sélection par des entiers positifs


```R
m <- matrix(1:12, ncol=4)
m
class(m)
```


<table>
<tbody>
	<tr><td>1 </td><td>4 </td><td>7 </td><td>10</td></tr>
	<tr><td>2 </td><td>5 </td><td>8 </td><td>11</td></tr>
	<tr><td>3 </td><td>6 </td><td>9 </td><td>12</td></tr>
</tbody>
</table>




'matrix'



```R
a <- m[2,]
a
dim(a)
```


<ol class=list-inline>
	<li>2</li>
	<li>5</li>
	<li>8</li>
	<li>11</li>
</ol>




    NULL



```R
b <- m[,1]
b
class(a) #a n'est pas un objet de classe "matrix"
```


<ol class=list-inline>
	<li>2</li>
	<li>5</li>
	<li>8</li>
	<li>11</li>
</ol>




<ol class=list-inline>
	<li>1</li>
	<li>2</li>
	<li>3</li>
</ol>




'integer'



```R
m
a <-m[2,,drop=F]
class(a) # a est un objet de classe "matrix"
a
dim(a)
```


<table>
<tbody>
	<tr><td>1 </td><td>4 </td><td>7 </td><td>10</td></tr>
	<tr><td>2 </td><td>5 </td><td>8 </td><td>11</td></tr>
	<tr><td>3 </td><td>6 </td><td>9 </td><td>12</td></tr>
</tbody>
</table>




'matrix'



<table>
<tbody>
	<tr><td>2 </td><td>5 </td><td>8 </td><td>11</td></tr>
</tbody>
</table>




<ol class=list-inline>
	<li>1</li>
	<li>4</li>
</ol>




```R
a <- m[,c(2,2,1)] # 2ieme, 2ieme, 1ere collonnes
m
a
```


<table>
<tbody>
	<tr><td>1 </td><td>4 </td><td>7 </td><td>10</td></tr>
	<tr><td>2 </td><td>5 </td><td>8 </td><td>11</td></tr>
	<tr><td>3 </td><td>6 </td><td>9 </td><td>12</td></tr>
</tbody>
</table>




<table>
<tbody>
	<tr><td>4</td><td>4</td><td>1</td></tr>
	<tr><td>5</td><td>5</td><td>2</td></tr>
	<tr><td>6</td><td>6</td><td>3</td></tr>
</tbody>
</table>



* Sélection par des entiers négatifs


```R
m <- matrix(1:8, ncol=4)
m
a <- m[-1,] # m sans sa premiere ligne
class(a)
a
```


<table>
<tbody>
	<tr><td>1</td><td>3</td><td>5</td><td>7</td></tr>
	<tr><td>2</td><td>4</td><td>6</td><td>8</td></tr>
</tbody>
</table>




'integer'



<ol class=list-inline>
	<li>2</li>
	<li>4</li>
	<li>6</li>
	<li>8</li>
</ol>




```R
m <- matrix(1:12, ncol=4)
m
a <- m[,-1] # m sans sa premiere ligne
a
class(a)

```


<table>
<tbody>
	<tr><td>1 </td><td>4 </td><td>7 </td><td>10</td></tr>
	<tr><td>2 </td><td>5 </td><td>8 </td><td>11</td></tr>
	<tr><td>3 </td><td>6 </td><td>9 </td><td>12</td></tr>
</tbody>
</table>




<table>
<tbody>
	<tr><td>4 </td><td>7 </td><td>10</td></tr>
	<tr><td>5 </td><td>8 </td><td>11</td></tr>
	<tr><td>6 </td><td>9 </td><td>12</td></tr>
</tbody>
</table>




'matrix'



```R
m <- matrix(1:8, ncol=4)
m
a <- m[1:2,-1] # 2 premieres lignes sans la 1ere colonne
a
class(a)
```


<table>
<tbody>
	<tr><td>1</td><td>3</td><td>5</td><td>7</td></tr>
	<tr><td>2</td><td>4</td><td>6</td><td>8</td></tr>
</tbody>
</table>




<table>
<tbody>
	<tr><td>3</td><td>5</td><td>7</td></tr>
	<tr><td>4</td><td>6</td><td>8</td></tr>
</tbody>
</table>




'matrix'



```R
a <- m[-1,1:2] # 2 premieres colonnes sans la 1ere ligne
a
class(a)
```


<ol class=list-inline>
	<li>2</li>
	<li>4</li>
</ol>




'integer'



```R
# 
m <- matrix(1:12, ncol=4)
m
a <- m[1:2,-1] # 2 premieres lignes sans la 1ere colonne
a
class(a)
```


<table>
<tbody>
	<tr><td>1 </td><td>4 </td><td>7 </td><td>10</td></tr>
	<tr><td>2 </td><td>5 </td><td>8 </td><td>11</td></tr>
	<tr><td>3 </td><td>6 </td><td>9 </td><td>12</td></tr>
</tbody>
</table>




<table>
<tbody>
	<tr><td>4 </td><td>7 </td><td>10</td></tr>
	<tr><td>5 </td><td>8 </td><td>11</td></tr>
</tbody>
</table>




'matrix'



```R
a <- m[-1,1:2] # 2 premieres colonnes sans la 1ere ligne
a
class(a)
```


<table>
<tbody>
	<tr><td>2</td><td>5</td></tr>
	<tr><td>3</td><td>6</td></tr>
</tbody>
</table>




'matrix'


### Calcul sur les matrices

* Les opérations "*","+", les fonctions se font terme à terme


```R
A <- matrix(1:4,ncol=2)
A
B <- matrix(3:6,ncol=2,byrow=T) # optimise les acces memoire
B
A+B # somme él"ment par element
```


<table>
<tbody>
	<tr><td>1</td><td>3</td></tr>
	<tr><td>2</td><td>4</td></tr>
</tbody>
</table>




<table>
<tbody>
	<tr><td>3</td><td>4</td></tr>
	<tr><td>5</td><td>6</td></tr>
</tbody>
</table>




<table>
<tbody>
	<tr><td>4 </td><td> 7</td></tr>
	<tr><td>7 </td><td>10</td></tr>
</tbody>
</table>




```R
A*B # produit element par eement
```


<table>
<tbody>
	<tr><td> 3</td><td>12</td></tr>
	<tr><td>10</td><td>24</td></tr>
</tbody>
</table>




```R
sin(A) # sinus element par element
exp(A) # exponentiellel element par element
```


<table>
<tbody>
	<tr><td>0.8414710 </td><td> 0.1411200</td></tr>
	<tr><td>0.9092974 </td><td>-0.7568025</td></tr>
</tbody>
</table>




<table>
<tbody>
	<tr><td>2.718282</td><td>20.08554</td></tr>
	<tr><td>7.389056</td><td>54.59815</td></tr>
</tbody>
</table>



### Opérateurs algébriques

Quelques fonctions algébriques sont disponibles dans l'installation de base de R. Pour connaître ce qui est disponible sur le sujet:

On a deux matrices **X** et **Y**

* **X%*%Y** : produit de matrices 
* **t(X)** : transposition d'une matrice 
* **diag(v)** : matrice diagonale de vecteur v 
* **crossprod(X,Y)** : produit t(X)%*%Y 
* **det(X)** : déterminant de la matrice X 
* **svd(X)** : décomposition en valeurs singulières 
* **eigen(X)** : diagonalisation d'une matrice 
* **solve(A,b)** résolution d'un système linéaire 
* **chol(Y)** décomposition de cholesky 
* **qr(Y)** Décomposition QR


```R
help.search("linear")
```


```R
# produit de deux matrices
A <- matrix(1:4,ncol=2)
B <- matrix(c(5,7,6,8),ncol=2)
D <- A%*%t(B) # produit des matrices
D
class(D)
```


<table>
<tbody>
	<tr><td>23</td><td>31</td></tr>
	<tr><td>34</td><td>46</td></tr>
</tbody>
</table>




'matrix'



```R
# Valeurs propres
eig <- eigen(D) # diagonalisation de D
eig
class(eig)
attributes(eig)
eig$values
class(eig$vectors)
eig$vectors[,1]
class(eig$values)
```


<dl>
	<dt>$values</dt>
		<dd><ol class=list-inline>
	<li>68.9419801985891</li>
	<li>0.0580198014109499</li>
</ol>
</dd>
	<dt>$vectors</dt>
		<dd><table>
<tbody>
	<tr><td>-0.5593385</td><td>-0.8038173</td></tr>
	<tr><td>-0.8289393</td><td> 0.5948762</td></tr>
</tbody>
</table>
</dd>
</dl>




'list'



<strong>$names</strong> = <ol class=list-inline>
	<li>'values'</li>
	<li>'vectors'</li>
</ol>




<ol class=list-inline>
	<li>68.9419801985891</li>
	<li>0.0580198014109499</li>
</ol>




'matrix'



<ol class=list-inline>
	<li>-0.559338522317071</li>
	<li>-0.828939332793514</li>
</ol>




'numeric'


### Opération sur les lignes et les colonnes (fonction apply)

* Les fonctions **ncol**,**nrow** et **dim**.


```R
A <- matrix(1:6,ncol=3)
ncol(A)
nrow(A)
dim(A)
```


3



2



<ol class=list-inline>
	<li>2</li>
	<li>3</li>
</ol>



* La fonction **apply** permet d'appliquer une fonction **f** aux lignes (MARGIN=1) ou aux colonnes (MARGIN=2) d'une matrice


```R
A <- matrix(1:6,ncol=3)
A
apply(A,MARGIN=2,sum) # somme par colonne
```


<table>
<tbody>
	<tr><td>1</td><td>3</td><td>5</td></tr>
	<tr><td>2</td><td>4</td><td>6</td></tr>
</tbody>
</table>




<ol class=list-inline>
	<li>3</li>
	<li>7</li>
	<li>11</li>
</ol>




```R
apply(A,MARGIN=1,mean) # moyenne par ligne
```


<ol class=list-inline>
	<li>3</li>
	<li>4</li>
</ol>



* Concaténation par colonne avec **cbind**, par ligne avec **rbind**


```R
mat1 <- matrix(1:8,ncol=4)
print(mat1)
mat2 <- matrix(1:12,ncol=4)
print(mat2)
```

         [,1] [,2] [,3] [,4]
    [1,]    1    3    5    7
    [2,]    2    4    6    8
         [,1] [,2] [,3] [,4]
    [1,]    1    4    7   10
    [2,]    2    5    8   11
    [3,]    3    6    9   12



```R
a <- rbind(mat1,mat2)
print(a)
class(a)
```

         [,1] [,2] [,3] [,4]
    [1,]    1    3    5    7
    [2,]    2    4    6    8
    [3,]    1    4    7   10
    [4,]    2    5    8   11
    [5,]    3    6    9   12



'matrix'


## Les facteurs

Les facteurs sont des **vecteurs** (*objet atomique*) permettant le traitement des
données **qualitatives**.

### Les attributs

Les attributs sont les deux attributes intrinsèques **mode** et **length** et l'attribut
**levels** qui contient les modalités du facteur.

Les modalités du facteur peuvent être obtenues grâce à la fonction **levels()**.
### Les constructeurs

Trois fonctions permettent de créer des facteurs:

* La fonction **factor()**


```R
sexe <- factor(c("M","F","M","M","M","F"))
sexe
levels(sexe)
attributes(sexe)
mode(sexe)
typeof(sexe)
class(sexe)
summary(sexe)
```


<ol class=list-inline>
	<li>M</li>
	<li>F</li>
	<li>M</li>
	<li>M</li>
	<li>M</li>
	<li>F</li>
</ol>




<ol class=list-inline>
	<li>'F'</li>
	<li>'M'</li>
</ol>




<dl>
	<dt>$levels</dt>
		<dd><ol class=list-inline>
	<li>'F'</li>
	<li>'M'</li>
</ol>
</dd>
	<dt>$class</dt>
		<dd>'factor'</dd>
</dl>




'numeric'



'integer'



'factor'



<dl class=dl-horizontal>
	<dt>F</dt>
		<dd>2</dd>
	<dt>M</dt>
		<dd>4</dd>
</dl>



* La fonction **ordered()**


```R
niveau <- ordered(c("deb","deb","cha","moyen","cha"),levels=c("deb","moyen","cha"))
niveau
levels(niveau)
class(niveau)
```


<ol class=list-inline>
	<li>deb</li>
	<li>deb</li>
	<li>cha</li>
	<li>moyen</li>
	<li>cha</li>
</ol>




<ol class=list-inline>
	<li>'deb'</li>
	<li>'moyen'</li>
	<li>'cha'</li>
</ol>




<ol class=list-inline>
	<li>'ordered'</li>
	<li>'factor'</li>
</ol>



* La fonction **as.factor()**


```R
# variable quantitative
salto <- c(1:5,7:3)
salto
class(salto)
summary(salto)
```


<ol class=list-inline>
	<li>1</li>
	<li>2</li>
	<li>3</li>
	<li>4</li>
	<li>5</li>
	<li>7</li>
	<li>6</li>
	<li>5</li>
	<li>4</li>
	<li>3</li>
</ol>




'integer'



       Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
          1       3       4       4       5       7 



```R
# variable qualitative
salto.f <- as.factor(salto)
salto.f
class(salto.f)
summary(salto.f)
```


<ol class=list-inline>
	<li>1</li>
	<li>2</li>
	<li>3</li>
	<li>4</li>
	<li>5</li>
	<li>7</li>
	<li>6</li>
	<li>5</li>
	<li>4</li>
	<li>3</li>
</ol>




'factor'



<dl class=dl-horizontal>
	<dt>1</dt>
		<dd>1</dd>
	<dt>2</dt>
		<dd>1</dd>
	<dt>3</dt>
		<dd>2</dd>
	<dt>4</dt>
		<dd>2</dd>
	<dt>5</dt>
		<dd>2</dd>
	<dt>6</dt>
		<dd>1</dd>
	<dt>7</dt>
		<dd>1</dd>
</dl>



### Sélection

* Pour retrouver les **modalités**, **le nombre de modalités**, **l'effectif par modalité**, on utilisera respectivement les fonctions **levels()**, **nlevels()** et **table()**:


```R
levels(salto.f)
nlevels(salto.f)
table(salto.f)
```


<ol class=list-inline>
	<li>'1'</li>
	<li>'2'</li>
	<li>'3'</li>
	<li>'4'</li>
	<li>'5'</li>
	<li>'6'</li>
	<li>'7'</li>
</ol>




7



    salto.f
    1 2 3 4 5 6 7 
    1 1 2 2 2 1 1 


 * La fonction **table** permet également de construire des tableaux croisés.
 
 Lorsque deux variables qualitatives sont traitées, on peut présenter les données sous deux formes:
    - par un **tableau classique** où une ligne représente un individu sur lequel est observées les deux variables en colonne.
    - par un **tableau croisé**, appelé aussi **tableau de contingence** qui donne l'effectif de chaque croisement des modalités.
    
Ce qui peut se généraliser à plus de deux variables qualitatives.


```R
# Deux variables qualitatives "laine" et "tension" sur 10 individus
tension <- factor(c(rep("Faible",5),rep("Forte",5)))
tension
levels(tension)
```


<ol class=list-inline>
	<li>Faible</li>
	<li>Faible</li>
	<li>Faible</li>
	<li>Faible</li>
	<li>Faible</li>
	<li>Forte</li>
	<li>Forte</li>
	<li>Forte</li>
	<li>Forte</li>
	<li>Forte</li>
</ol>




<ol class=list-inline>
	<li>'Faible'</li>
	<li>'Forte'</li>
</ol>




```R
laine  <- factor(c(rep("Mer",3),rep("Ang",3),rep("Tex",4)))
laine
levels(laine)
```


<ol class=list-inline>
	<li>Mer</li>
	<li>Mer</li>
	<li>Mer</li>
	<li>Ang</li>
	<li>Ang</li>
	<li>Ang</li>
	<li>Tex</li>
	<li>Tex</li>
	<li>Tex</li>
	<li>Tex</li>
</ol>




<ol class=list-inline>
	<li>'Ang'</li>
	<li>'Mer'</li>
	<li>'Tex'</li>
</ol>




```R
# On fusionne les deux variables dans le tableau don avec cbind
ls()
don <- cbind(tension,laine)
don
class(don)
summary(don)
```


<ol class=list-inline>
	<li>'don'</li>
	<li>'laine'</li>
	<li>'tension'</li>
</ol>




<table>
<thead><tr><th scope=col>tension</th><th scope=col>laine</th></tr></thead>
<tbody>
	<tr><td>1</td><td>2</td></tr>
	<tr><td>1</td><td>2</td></tr>
	<tr><td>1</td><td>2</td></tr>
	<tr><td>1</td><td>1</td></tr>
	<tr><td>1</td><td>1</td></tr>
	<tr><td>2</td><td>1</td></tr>
	<tr><td>2</td><td>3</td></tr>
	<tr><td>2</td><td>3</td></tr>
	<tr><td>2</td><td>3</td></tr>
	<tr><td>2</td><td>3</td></tr>
</tbody>
</table>




'matrix'



        tension        laine     
     Min.   :1.0   Min.   :1.00  
     1st Qu.:1.0   1st Qu.:1.25  
     Median :1.5   Median :2.00  
     Mean   :1.5   Mean   :2.10  
     3rd Qu.:2.0   3rd Qu.:3.00  
     Max.   :2.0   Max.   :3.00  



```R
don <- as.data.frame(don)  # cbind.data.frame
class(don)
attributes(don)
summary(don)
don$tension
```


'data.frame'



<dl>
	<dt>$names</dt>
		<dd><ol class=list-inline>
	<li>'tension'</li>
	<li>'laine'</li>
</ol>
</dd>
	<dt>$row.names</dt>
		<dd><ol class=list-inline>
	<li>1</li>
	<li>2</li>
	<li>3</li>
	<li>4</li>
	<li>5</li>
	<li>6</li>
	<li>7</li>
	<li>8</li>
	<li>9</li>
	<li>10</li>
</ol>
</dd>
	<dt>$class</dt>
		<dd>'data.frame'</dd>
</dl>




        tension        laine     
     Min.   :1.0   Min.   :1.00  
     1st Qu.:1.0   1st Qu.:1.25  
     Median :1.5   Median :2.00  
     Mean   :1.5   Mean   :2.10  
     3rd Qu.:2.0   3rd Qu.:3.00  
     Max.   :2.0   Max.   :3.00  



<ol class=list-inline>
	<li>1</li>
	<li>1</li>
	<li>1</li>
	<li>1</li>
	<li>1</li>
	<li>2</li>
	<li>2</li>
	<li>2</li>
	<li>2</li>
	<li>2</li>
</ol>




```R
# On fusionne les deux variables dans le tableau don avec cbind.data.frame
don2 <- cbind.data.frame(tension,laine)
don2
class(don2)
attributes(don2)
don2$tension
```


<table>
<thead><tr><th scope=col>tension</th><th scope=col>laine</th></tr></thead>
<tbody>
	<tr><td>Faible</td><td>Mer   </td></tr>
	<tr><td>Faible</td><td>Mer   </td></tr>
	<tr><td>Faible</td><td>Mer   </td></tr>
	<tr><td>Faible</td><td>Ang   </td></tr>
	<tr><td>Faible</td><td>Ang   </td></tr>
	<tr><td>Forte </td><td>Ang   </td></tr>
	<tr><td>Forte </td><td>Tex   </td></tr>
	<tr><td>Forte </td><td>Tex   </td></tr>
	<tr><td>Forte </td><td>Tex   </td></tr>
	<tr><td>Forte </td><td>Tex   </td></tr>
</tbody>
</table>




'data.frame'



<dl>
	<dt>$names</dt>
		<dd><ol class=list-inline>
	<li>'tension'</li>
	<li>'laine'</li>
</ol>
</dd>
	<dt>$row.names</dt>
		<dd><ol class=list-inline>
	<li>1</li>
	<li>2</li>
	<li>3</li>
	<li>4</li>
	<li>5</li>
	<li>6</li>
	<li>7</li>
	<li>8</li>
	<li>9</li>
	<li>10</li>
</ol>
</dd>
	<dt>$class</dt>
		<dd>'data.frame'</dd>
</dl>




<ol class=list-inline>
	<li>Faible</li>
	<li>Faible</li>
	<li>Faible</li>
	<li>Faible</li>
	<li>Faible</li>
	<li>Forte</li>
	<li>Forte</li>
	<li>Forte</li>
	<li>Forte</li>
	<li>Forte</li>
</ol>



Observez la différences entre **don** et **don2** 

On conserve **don2**

* Construction du tableau croisé avec la fonction **table**.


```R
tabcroise <- table(don2$laine,don2$tension)
class(tabcroise)
tabcroise
```


'table'



         
          Faible Forte
      Ang      2     1
      Mer      3     0
      Tex      0     4


De nombreuses fonctions de R étant bâties sur l'hypothèse d'un tableau **individiXvariables**, il faut pouvoir effectuer l'opération inverse lorsque les **données sont fournies sous forme de tableau de contingence**.


```R
tabframe <- as.data.frame(tabcroise)
tabframe
attributes(tabframe)
summary(tabframe)
# construire le tableau de donnees individusXvariables à partir de tabframe est possible
# petit exercice de programmation
```


<table>
<thead><tr><th scope=col>Var1</th><th scope=col>Var2</th><th scope=col>Freq</th></tr></thead>
<tbody>
	<tr><td>Ang   </td><td>Faible</td><td>2     </td></tr>
	<tr><td>Mer   </td><td>Faible</td><td>3     </td></tr>
	<tr><td>Tex   </td><td>Faible</td><td>0     </td></tr>
	<tr><td>Ang   </td><td>Forte </td><td>1     </td></tr>
	<tr><td>Mer   </td><td>Forte </td><td>0     </td></tr>
	<tr><td>Tex   </td><td>Forte </td><td>4     </td></tr>
</tbody>
</table>




<dl>
	<dt>$names</dt>
		<dd><ol class=list-inline>
	<li>'Var1'</li>
	<li>'Var2'</li>
	<li>'Freq'</li>
</ol>
</dd>
	<dt>$row.names</dt>
		<dd><ol class=list-inline>
	<li>1</li>
	<li>2</li>
	<li>3</li>
	<li>4</li>
	<li>5</li>
	<li>6</li>
</ol>
</dd>
	<dt>$class</dt>
		<dd>'data.frame'</dd>
</dl>




      Var1       Var2        Freq      
     Ang:2   Faible:3   Min.   :0.000  
     Mer:2   Forte :3   1st Qu.:0.250  
     Tex:2              Median :1.500  
                        Mean   :1.667  
                        3rd Qu.:2.750  
                        Max.   :4.000  


## Les liste

Les listes sont des **objets hétérogènes**. C'est un ensemble ordonné d'objets ou composantes qui n'ont pas toujours le même mode ou la même longueur.

Les listes sont des objets importants car toutes les fonctions qui retournent plusieurs objets le font sous la forme d'une liste.

### Les attributs:

Les deux attributes intrinsèques **mode** et **length** et l'attribut
supplémentaires **names** qui contient le nom des composantes de la liste.

Le nom des composantes peut être défini et obtenu par la fonctions **names()**.

### Constructeurs

La fonction **list()** permet de créer une liste:



```R
vecteur <- c(2,10,by=2)
matrice <- matrix(1:8,ncol=2)
facteur <- factor(c("M","F","M","M","M","F"))
maListe <- list(vecteur,matrice,facteur)
mode(maListe)
length(maListe)
attributes(maListe)
names(maListe) <-c("vecteur","matrice","facteur")
attributes(maListe)
maListe
```


'list'



3



    NULL



<strong>$names</strong> = <ol class=list-inline>
	<li>'vecteur'</li>
	<li>'matrice'</li>
	<li>'facteur'</li>
</ol>




<dl>
	<dt>$vecteur</dt>
		<dd><dl class=dl-horizontal>
	<dt>1</dt>
		<dd>2</dd>
	<dt>2</dt>
		<dd>10</dd>
	<dt>by</dt>
		<dd>2</dd>
</dl>
</dd>
	<dt>$matrice</dt>
		<dd><table>
<tbody>
	<tr><td>1</td><td>5</td></tr>
	<tr><td>2</td><td>6</td></tr>
	<tr><td>3</td><td>7</td></tr>
	<tr><td>4</td><td>8</td></tr>
</tbody>
</table>
</dd>
	<dt>$facteur</dt>
		<dd><ol class=list-inline>
	<li>M</li>
	<li>F</li>
	<li>M</li>
	<li>M</li>
	<li>M</li>
	<li>F</li>
</ol>
</dd>
</dl>



### Extraction d'une liste

Pour extraire une composante de la liste:

* On utilise l'opérateur "**[[]]**" (help("[[")) en indiquant la
position de la composante qu'on veut extraire. On peut aussi utiliser le **nom de la composante**.


```R
#maListe
d <-maListe[[2]]
d
class(d)
maListe[[2]][2,1] # l'element de la deuxieme ligne et premiere colonne de la composante "matrice"
```


<table>
<tbody>
	<tr><td>1</td><td>5</td></tr>
	<tr><td>2</td><td>6</td></tr>
	<tr><td>3</td><td>7</td></tr>
	<tr><td>4</td><td>8</td></tr>
</tbody>
</table>




'matrix'



2


* On peut aussi utiliser le **nom de la composante**.


```R
attributes(maListe)
maListe$vecteur
maListe[["vecteur"]]
```


<strong>$names</strong> = <ol class=list-inline>
	<li>'vecteur'</li>
	<li>'matrice'</li>
	<li>'facteur'</li>
</ol>




<dl class=dl-horizontal>
	<dt>1</dt>
		<dd>2</dd>
	<dt>2</dt>
		<dd>10</dd>
	<dt>by</dt>
		<dd>2</dd>
</dl>




<dl class=dl-horizontal>
	<dt>1</dt>
		<dd>2</dd>
	<dt>2</dt>
		<dd>10</dd>
	<dt>by</dt>
		<dd>2</dd>
</dl>



* On peut extraire plusieurs élément d'une même liste en utilisant l'opérateur "**[]**", on obtient une liste.


```R
a <- maListe[2]
class(a)
attributes(a)
```


'list'



<strong>$names</strong> = 'matrice'



```R
b <- maListe[[2]]
class(b)
```


'matrix'



```R
c <- maListe[c(1,3)]
c
class(c)
```


<dl>
	<dt>$vecteur</dt>
		<dd><dl class=dl-horizontal>
	<dt>1</dt>
		<dd>2</dd>
	<dt>2</dt>
		<dd>10</dd>
	<dt>by</dt>
		<dd>2</dd>
</dl>
</dd>
	<dt>$facteur</dt>
		<dd><ol class=list-inline>
	<li>M</li>
	<li>F</li>
	<li>M</li>
	<li>M</li>
	<li>M</li>
	<li>F</li>
</ol>
</dd>
</dl>




'list'


### Quelques fonctions sur les listes

* la fonction **lapply**: applique une fonction(moyenne, variance,...) à chacune des composantes de la liste.


```R
x <- list(a = 1:10, beta = exp(-3:3),logic = c(TRUE,FALSE,FALSE,TRUE))
# Calcule la moyenne pour chaque element de la liste
xmean <-lapply(x, mean) # median for each list element
xmean
class(xmean)
xmean$a
xmean$beta
xmean$logic
```


<dl>
	<dt>$a</dt>
		<dd>5.5</dd>
	<dt>$beta</dt>
		<dd>4.53512523476476</dd>
	<dt>$logic</dt>
		<dd>0.5</dd>
</dl>




'list'



5.5



4.53512523476476



0.5


* Pour concaténer deux listes, on utilise la fonction "**c()**"


```R
l1 <- list(a = 1:10, beta = exp(-3:3))
length(l1)
l2 <- list(ch=c("M","A","A"), re=seq(0,1,20))
length(l2)
ll <- c(l1,l2)
mode(ll)
length(ll)
class(ll)
names(l1) 
names(ll)
```


2



2



'list'



4



'list'



<ol class=list-inline>
	<li>'a'</li>
	<li>'beta'</li>
</ol>




<ol class=list-inline>
	<li>'a'</li>
	<li>'beta'</li>
	<li>'ch'</li>
	<li>'re'</li>
</ol>



### La liste **dimnames**

C'est un attribut optionnel d'un objet R à plusieurs dimensions(matrices, array,list,...) qui contient **dans une liste** les noms des éléments de chaque dimension.

* Cas d'une matrice


```R
# Cas d'une matrice
X <- matrix(1:12,nrow=4) # 4 lignes et 3 colonnes
nomligne <- c("l1","l2","l3","l4")
nomcol <- c("col1","col2","col3")
X
dimnames(X) <- list(nomligne,nomcol)
X
```


<table>
<tbody>
	<tr><td>1 </td><td>5 </td><td> 9</td></tr>
	<tr><td>2 </td><td>6 </td><td>10</td></tr>
	<tr><td>3 </td><td>7 </td><td>11</td></tr>
	<tr><td>4 </td><td>8 </td><td>12</td></tr>
</tbody>
</table>




<table>
<thead><tr><th></th><th scope=col>col1</th><th scope=col>col2</th><th scope=col>col3</th></tr></thead>
<tbody>
	<tr><th scope=row>l1</th><td>1 </td><td>5 </td><td> 9</td></tr>
	<tr><th scope=row>l2</th><td>2 </td><td>6 </td><td>10</td></tr>
	<tr><th scope=row>l3</th><td>3 </td><td>7 </td><td>11</td></tr>
	<tr><th scope=row>l4</th><td>4 </td><td>8 </td><td>12</td></tr>
</tbody>
</table>



* Il est possible de sélectionner des lognes ou des colonnes avec l'attribut dimnanes.


```R
X[c("l1","l4"),c("col2","col3")]
```


<table>
<thead><tr><th></th><th scope=col>col2</th><th scope=col>col3</th></tr></thead>
<tbody>
	<tr><th scope=row>l1</th><td>5 </td><td> 9</td></tr>
	<tr><th scope=row>l4</th><td>8 </td><td>12</td></tr>
</tbody>
</table>



## Les data-frame
Un tableau de données est un ensemble de vecteurs rangés colonne par colonne. Chaque colonne du tableau data correspond à une variable, chaque ligne à un individu. Les variables peuvent être de type différent (numérique, booléen, character,...), elles sont de même longueur.

C'est ce même concept qu'on utilise dans les progiciels statistiques tels que SAS, BMDP, SPSS...



Les **data-frame** sont des **listes particulières** dont les composantes sont de même
longueur mais dont les modes peuvent être différents. 

Les tableaux de données usuels utiliséss en statistique sont considérés comme des data-frame.

### Les attributs

Un **data-frame** a pour attributs son **mode**, sa **longueur**, le **nom des variables** (names), le **nom des individus** (row.names) et les **dimensions du tableau** (nombre de lignes, nombre de colonnes).

Pour accéder à la valeur de ces attributs, on utilise le caractère **\$** et le nom de l'attribut.

La fonction **dim()** fournit les dimensions du tableau de données: le nombre d'observations et le nombre de variables.

La fonction **dimnames()** fournit le nom des dimensions du tableau de données: le nom des individus et le nom des variables.

La fonction **str()** fournit l'ensemble des informations sur les variables du data-frame en argument.

### Les constructeurs

Les principales manières de construire un data-frame est d'utiliser une des fonctions:

* **data.frame** qui permet de concaténer des vecteurs de même taille.



```R
n <- 5
vec1 <- 1:n
vec2 <- c("a","f","s","o","p")
df <- data.frame(nomVar1=vec1,nomVar2=vec2)
df
```


<table>
<thead><tr><th scope=col>nomVar1</th><th scope=col>nomVar2</th></tr></thead>
<tbody>
	<tr><td>1</td><td>a</td></tr>
	<tr><td>2</td><td>f</td></tr>
	<tr><td>3</td><td>s</td></tr>
	<tr><td>4</td><td>o</td></tr>
	<tr><td>5</td><td>p</td></tr>
</tbody>
</table>




```R
str(df)
class(df)
length(df) # nombre de variables
dim(df) # dimension du tableau de donnees
```

    'data.frame':	5 obs. of  2 variables:
     $ nomVar1: int  1 2 3 4 5
     $ nomVar2: Factor w/ 5 levels "a","f","o","p",..: 1 2 5 3 4



'data.frame'



2



<ol class=list-inline>
	<li>5</li>
	<li>2</li>
</ol>




```R
dimnames(df)
dimnames(df)[[1]] # noms des lignes
dimnames(df)[[2]] # noms des colonnes
```


<ol>
	<li><ol class=list-inline>
	<li>'1'</li>
	<li>'2'</li>
	<li>'3'</li>
	<li>'4'</li>
	<li>'5'</li>
</ol>
</li>
	<li><ol class=list-inline>
	<li>'nomVar1'</li>
	<li>'nomVar2'</li>
</ol>
</li>
</ol>




<ol class=list-inline>
	<li>'1'</li>
	<li>'2'</li>
	<li>'3'</li>
	<li>'4'</li>
	<li>'5'</li>
</ol>




<ol class=list-inline>
	<li>'nomVar1'</li>
	<li>'nomVar2'</li>
</ol>



* En utilisant la fonction **read.table**, **read.csv**,... lit un tableau de données et crée un **data-frame**


```R
mtcars <- read.csv("data/mtcars.csv",header=TRUE,sep=",",dec=".")
class(mtcars)
dim(mtcars)
dimnames(mtcars)
```


'data.frame'



<ol class=list-inline>
	<li>32</li>
	<li>11</li>
</ol>




<ol>
	<li><ol class=list-inline>
	<li>'1'</li>
	<li>'2'</li>
	<li>'3'</li>
	<li>'4'</li>
	<li>'5'</li>
	<li>'6'</li>
	<li>'7'</li>
	<li>'8'</li>
	<li>'9'</li>
	<li>'10'</li>
	<li>'11'</li>
	<li>'12'</li>
	<li>'13'</li>
	<li>'14'</li>
	<li>'15'</li>
	<li>'16'</li>
	<li>'17'</li>
	<li>'18'</li>
	<li>'19'</li>
	<li>'20'</li>
	<li>'21'</li>
	<li>'22'</li>
	<li>'23'</li>
	<li>'24'</li>
	<li>'25'</li>
	<li>'26'</li>
	<li>'27'</li>
	<li>'28'</li>
	<li>'29'</li>
	<li>'30'</li>
	<li>'31'</li>
	<li>'32'</li>
</ol>
</li>
	<li><ol class=list-inline>
	<li>'mpg'</li>
	<li>'cyl'</li>
	<li>'disp'</li>
	<li>'hp'</li>
	<li>'drat'</li>
	<li>'wt'</li>
	<li>'qsec'</li>
	<li>'vs'</li>
	<li>'am'</li>
	<li>'gear'</li>
	<li>'carb'</li>
</ol>
</li>
</ol>




```R
mtcars2 <- read.table("data/mtcars.csv",header=TRUE,sep=",")
class(mtcars2)
dim(mtcars2)
dimnames(mtcars2)
```


'data.frame'



<ol class=list-inline>
	<li>32</li>
	<li>11</li>
</ol>




<ol>
	<li><ol class=list-inline>
	<li>'1'</li>
	<li>'2'</li>
	<li>'3'</li>
	<li>'4'</li>
	<li>'5'</li>
	<li>'6'</li>
	<li>'7'</li>
	<li>'8'</li>
	<li>'9'</li>
	<li>'10'</li>
	<li>'11'</li>
	<li>'12'</li>
	<li>'13'</li>
	<li>'14'</li>
	<li>'15'</li>
	<li>'16'</li>
	<li>'17'</li>
	<li>'18'</li>
	<li>'19'</li>
	<li>'20'</li>
	<li>'21'</li>
	<li>'22'</li>
	<li>'23'</li>
	<li>'24'</li>
	<li>'25'</li>
	<li>'26'</li>
	<li>'27'</li>
	<li>'28'</li>
	<li>'29'</li>
	<li>'30'</li>
	<li>'31'</li>
	<li>'32'</li>
</ol>
</li>
	<li><ol class=list-inline>
	<li>'mpg'</li>
	<li>'cyl'</li>
	<li>'disp'</li>
	<li>'hp'</li>
	<li>'drat'</li>
	<li>'wt'</li>
	<li>'qsec'</li>
	<li>'vs'</li>
	<li>'am'</li>
	<li>'gear'</li>
	<li>'carb'</li>
</ol>
</li>
</ol>



* En utilisant la fonction **as.data.frame** pour effectuer une **conversion explicite**.
* Pour transformer une **matrice en data-frame**, on utilise la fonction **as.data.frame**.
* Pour transformer un **data-frame en une matrice**, on utilise la fonction **as.matrix**.

### Fonctions utiles dans un data-frame

* **head()** - pour voir les 6 premières lignes 
* **tail()** - pour voir les 6 dernières lignes 
* **dim()** - ses dimensions 
* **nrow()** - le nombre de lignes 
* **ncol()** - le nombre de colonnes 
* **str()** - structure de chaque colonne
* **names()** - liste l'attribut **names** d'un data.frame (ou n'importe quel autre objet), les noms des colonnes
* **dimnanes()** - liste l'attribut **row.names** d'un data.frame.

### Extraction

Pour extraire les composantes des data-frame, on peut utiliser les **méthodes pour les listes ou pour les matrices**.



```R
# une variable du data-frame
names(mtcars)  # noms des variables
#str(mtcars)
mtcars$mpg      # affichage du contenu de la variable mtg par son nom
```


<ol class=list-inline>
	<li>'mpg'</li>
	<li>'cyl'</li>
	<li>'disp'</li>
	<li>'hp'</li>
	<li>'drat'</li>
	<li>'wt'</li>
	<li>'qsec'</li>
	<li>'vs'</li>
	<li>'am'</li>
	<li>'gear'</li>
	<li>'carb'</li>
</ol>




<ol class=list-inline>
	<li>21</li>
	<li>21</li>
	<li>22.8</li>
	<li>21.4</li>
	<li>18.7</li>
	<li>18.1</li>
	<li>14.3</li>
	<li>24.4</li>
	<li>22.8</li>
	<li>19.2</li>
	<li>17.8</li>
	<li>16.4</li>
	<li>17.3</li>
	<li>15.2</li>
	<li>10.4</li>
	<li>10.4</li>
	<li>14.7</li>
	<li>32.4</li>
	<li>30.4</li>
	<li>33.9</li>
	<li>21.5</li>
	<li>15.5</li>
	<li>15.2</li>
	<li>13.3</li>
	<li>19.2</li>
	<li>27.3</li>
	<li>26</li>
	<li>30.4</li>
	<li>15.8</li>
	<li>19.7</li>
	<li>15</li>
	<li>21.4</li>
</ol>




```R
mtcars[[1]] # affichage du contenu de la variable mtg par son numéro d'ordre dans le data-frame
```


<ol class=list-inline>
	<li>21</li>
	<li>21</li>
	<li>22.8</li>
	<li>21.4</li>
	<li>18.7</li>
	<li>18.1</li>
	<li>14.3</li>
	<li>24.4</li>
	<li>22.8</li>
	<li>19.2</li>
	<li>17.8</li>
	<li>16.4</li>
	<li>17.3</li>
	<li>15.2</li>
	<li>10.4</li>
	<li>10.4</li>
	<li>14.7</li>
	<li>32.4</li>
	<li>30.4</li>
	<li>33.9</li>
	<li>21.5</li>
	<li>15.5</li>
	<li>15.2</li>
	<li>13.3</li>
	<li>19.2</li>
	<li>27.3</li>
	<li>26</li>
	<li>30.4</li>
	<li>15.8</li>
	<li>19.7</li>
	<li>15</li>
	<li>21.4</li>
</ol>



Pour en savoir plus [Gestion des données avec R](https://www.fun-mooc.fr/c4x/UPSUD/42001S03/asset/data-management.html) (Christophe Lalanne & Bruno Falissard -MOOC "Introduction à la statistique avec R").

## Les fonctions

Une **fonction** permet d'effectuer un certain nombre d'instructions R.

* Elle peut dépendre d'une **liste d'arguments**, éventuellement vide, en entrée. 
* Un argument fournit sous la forme **nom = valeur** permet de donner une valeur par défaut à cet argument.
* Elle fournitun **résultat unique** en sortie transmit par la fonction **return**.
* Par défaut, en l'absence d'appel à **return**, le dernier résultat obtenu avant la sortie de la fonction est retourné comme résultat.

L'implémentation de R de base fournit un grand nombre de fonctions prédéfinies (mean, max, ...). Voir **help("Math")**.

Une nouvelle fonction est créée par une construction de la forme :

    fun.name<-function( *arglist* ) {
        bloc d'instructions
        }
Une fonction est un **objet** R qui devra appartenir au **workspace** de la session pour être utilisée. 

* R fournit des fonctions prédéfinies, pour en connaître la liste:



```R
help("Math")
```


```R
rm(list=ls()) # on efface tous les objets de l'espace de travail
```


```R
# Si la fonction "som" est dans un script R stocké dans le fichier somme.R
ls()
source("somme.R")
# La fonction "som" est un objet de l'espace de travail
ls() 
som(6)
```






'som'



21



```R
# La fonction "som" n'est pas un objet de l'espace de travail
rm(som)
ls() 
som(6)
```






    Error in eval(expr, envir, enclos): could not find function "som"
    Traceback:




```R
"avant"
ls()
som <- function(n){
    result <- sum(1:n)
    return(result)
}
"apres"
ls()
som(3)  # appel a la fonction
```


'avant'







'apres'



'som'



6


### Les arguments d'une fonction

* Des valeurs par défaut sont données aux paramètres **a** et **b**.


```R
mafonction <- function(a = 1, b = 2, c) {
    resultat <- c(a, b, c)
    names(resultat) <- c("a", "b", "c")
    return(resultat) }
mafonction(6,7,8)
```


```R
# Valeur par défaut pour b
mafonction(10,c="string")
```


<dl class=dl-horizontal>
	<dt>a</dt>
		<dd>'10'</dd>
	<dt>b</dt>
		<dd>'2'</dd>
	<dt>c</dt>
		<dd>'string'</dd>
</dl>




```R
# Valeur par défaut pour a
mafonction(b=10,c="string")
```


<dl class=dl-horizontal>
	<dt>a</dt>
		<dd>'1'</dd>
	<dt>b</dt>
		<dd>'10'</dd>
	<dt>c</dt>
		<dd>'string'</dd>
</dl>




```R
mafonction(10,"string")
```


    Error in mafonction(10, "string"): argument "c" is missing, with no default
    Traceback:


    1. mafonction(10, "string")


* la fonction **args()** fournit la liste des **arguments** d'une fonction.


```R
args(mafonction)
```


<pre class=language-r><code>function (a = 1, b = 2, c) 
NULL</code></pre>



```R
# arguments par défaut de la fonction plot
args(plot.default)
```


<pre class=language-r><code>function (x, y = NULL, type = "p", xlim = NULL, ylim = NULL, 
<span style=white-space:pre-wrap>    log = "", main = NULL, sub = NULL, xlab = NULL, ylab = NULL, </span>
<span style=white-space:pre-wrap>    ann = par("ann"), axes = TRUE, frame.plot = axes, panel.first = NULL, </span>
<span style=white-space:pre-wrap>    panel.last = NULL, asp = NA, ...) </span>
NULL</code></pre>


La simple consultation de la liste des arguments remplace parfois avantageusement la lecture de la documentation.

* La fonction **body()** fournit le **corp de la fonction** donnée en argument.
* On peut aussi utilser le nom de la fonction sans les parenthèses, on aura en sortie le résultat de **args() + body()**


```R
body(mafonction)
```


    {
        resultat <- c(a, b, c)
        names(resultat) <- c("a", "b", "c")
        return(resultat)
    }



```R
mafonction # args() + body()
```


<pre class=language-r><code>function (a = 1, b = 2, c) 
{
<span style=white-space:pre-wrap>    resultat &lt;- c(a, b, c)</span>
<span style=white-space:pre-wrap>    names(resultat) &lt;- c("a", "b", "c")</span>
<span style=white-space:pre-wrap>    return(resultat)</span>
}</code></pre>


### L'argument "..."

* L'argument **point-point-point** permet à une fonction d'accepter un nombre quelconque d'arguments.
* L'argument **point-point-point** (.  .  .  )  indique que la fonction accepte n'importe quoi d'autre comme argument. Ce qu'il adviendra de ces arguments est déterminé par la fonction.
* En général ils sont transmis à d'autres fonctions.

Par exemple, une fonction graphique de haut niveau transmettra l'argument point-point-point à des fonctions graphiques de bas niveau pour traitement.



```R
# Utilisation de l'argument "..."
f <- function(x, y, ...) {
    plot.default(x = x, y = y, ...)
    abline(coef = lm(y ~ x)$coef)
}
f(1:10, 1:10)

```


![png](Objets_files/Objets_171_0.png)


Comme nous avons transmis l'argument **point-point-point** à **plot.default()**, tout ce que **plot.default()** sait faire, notre fonction **f()** sait le faire également.


```R
f(1:10, 1:10, main = "Titre de ce graphique", pch = 19,col = "blue", las = 1, 
  panel.first = grid(col = "pink",lty = "solid"))
```


![png](Objets_files/Objets_173_0.png)


### Plusieurs résultats à une même fonction

Le **résultat** sera fourni **sous forme d'une liste**. 

Considérons une fonction avec deux arguments en entrée: *facteur* et *facteur2*, deux variables qualitatives.

Cette fonction (mafonc) renvoie le *tableau de contingence* et *le vecteur de caractères des niveaux de facteur1 et facteurs2 pour lesquels l'effectif est conjointement nul*.


```R
# Definition de la fonction
mafonc <- function(facteur1,facteur2) {
    res1 <-table(facteur1,facteur2)  # tableau de contingence
    selection <- which(res1 == 0,arr.ind=TRUE)
    res2 <- matrix("",nrow=nrow(selection),ncol=2)
    res2[,1] <- levels(facteur1)[selection[,1]]
    res2[,2] <- levels(facteur2)[selection[,2]]
    # deux objets à retourner, utilisation d'une liste
    return(list(tab=res1,niveau=res2))}

```


```R
# Appel de la fonction
tension <- factor(c(rep("Faible",5),rep("Forte",5)))
laine <- factor(c(rep("Mer",3),rep("Ang",3),rep("Tex",4)))
#
res <- mafonc(tension,laine)
class(res)
res$tab
res$niveau
```


'list'



            facteur2
    facteur1 Ang Mer Tex
      Faible   2   3   0
      Forte    1   0   4



<table>
<tbody>
	<tr><td>Forte </td><td>Mer   </td></tr>
	<tr><td>Faible</td><td>Tex   </td></tr>
</tbody>
</table>



### Portée des variables

* A l'**intérieur d'une fonction** la variable est d'abord recherchée à l'**intérieur de la fonction**, 
à savoir:

   - Les variables définies comme arguments de cette fonction.
   - Les variables définies à l'intérieur de la fonction.
    
* Si une variable n'est pas trouvée à **l'intérieur de la fonction**, elle est recherchée **en dehors de la fonction**.

* Une variable **définie à l'extérieur** de la fonction est accessible aussi **dans la fonction**.

* Si deux variables avec le même nom sont définies **à l'intérieur et à l'extérieur de la fonction**, c'est la **variable locale** qui sera utilisée.

* Une erreur aura lieu si aucune variable avec le nom demandé n'est trouvée.



```R
rm(list=ls())
mavariable <- 1
ls()
#
# la variable "ma variable" est définie à l'extérieur et à l'intérieur de la fonction
mafonction1 <- function() {
    mavariable <- 5
    print(mavariable)
} 
#
mafonction1()
mavariable
#
```


'mavariable'


    [1] 5



1



```R
# la variable "ma variable" est définie uniquement à l'extérieur
mafonction2 <- function() {
    print(mavariable)
}
mafonction2()
```

    [1] 1

