## Présentation du cours

Ce cours est une introduction à R et RStudio à destination d'ATMO Auvergne Rhône-Alpes. Il a pour objectif de savoir utiliser des scripts R pour 
analyser des masses de données importantes et en sortir des informations sous forme de tableaux, de graphiques ou de cartographie.  

A l'issue de ce cours, les participants devront être capables de:

- Maîtriser les éléments fondamentaux du langage R.
- Ecrire un code simple.
- Utiliser des procédures statistiques ou de manipulation de données issues de packages existants.
- Déchiffrer et utiliser un code plus compliqué (déjà écrit).
- Savoir interfacer R avec Excel ou d'autres formats propriétaires (SAS,SPSS,...).
- Savoir comment représenter les données sous forme graphique et/ou cartographique.

Une **première partie** aborde les raisons du choix de ce logiciel pour le traitement des données, son installation, la documentation,
les packages, les interfaces utilisateurs disponibles (IDE) et une description de RStudio, l'IDE choisit pour ce cours. (IntroR.ipynb,IntroR.Rnw). 
On abordera quelques utilisations de bases.

Dans une **deuxième partie** on présente les concepts de bases : les objets, les fonctions,... (Objet.ipynb, Objet.Rnw).

Dans la **troisième partie**, on aborde les manipulations de données qui sont des opérations courantes dans la pratique statistique: 
importation de données sous différents formats propriétaires, sauvegarde de données et de résultats, traitements des données manquantes, concaténation des niveaux de 
facteurs,... (ManipDon.ipynb, ManipDon.Rnw). On effectuera quelques traitements statistiques sur les données.

Le traitement statistique s'accompagne d'une visualisation claire et synthétique des résultats, nous décrivons dans la **quatrième partie**
les nombreuses possibilités offertes par R dans ce domaine (GraphiqueR.ipynb, GraphiqueR.Rnw).

Enfin dans la **cinquième partie**, nous présentons quelques éléments de programmation, comment construire un script R implémentant
un algorithme statistique ou une chaine de  traitements sur des données. On aborde dans cette partie les capacités de déboguage 
et de profiling de R et rapidement comment utiliser les ressources multi-coeur actuellement disponibles sur les portables ou 
serveurs à notre disposition (ProgrammerR.ipynb, ProgrammerR.Rnw).

Tout au long de ce cours on fera référence au package **tidyverse**. Le tidyverse est un ensemble d’extensions pour R construites autour d’une philosophie 
commune facilitant l’utilisation de R dans les domaines les plus courants comme la manipulation des données, le recodage, la production de graphiques, etc.
( à consulter: [Introduction à R et au tidyverse](https://juba.github.io/tidyverse/index.html))

Pour chacune de ces parties, vous aurez des fichiers notebook que vous récupérerez sur le serveur GITLAB de GRICAD, un fichier 
[R notebook Markdown](http://rmarkdown.rstudio.com/r_notebooks.html) et une version **pdf*. 

[Pour en savoir plus sur les notebooks (jupyter, Rmarkdown)](https://www.datacamp.com/community/blog/jupyter-notebook-r#gs.6iMBYHw)

Pour l'accès aux serveur GITLAB et au serveur de notebooks de GRICAD, voir **"Informations pratiques"**

### Travailler sur des données personnelles

Nous travaillerons sur des données fournies par ATMO.
