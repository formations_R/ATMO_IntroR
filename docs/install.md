## Installation de R sur votre portable

Pour un meilleur suivi du cours, nous vous conseillons d'installer R (Version 3.4.x au moins) et [RStudio](https://www.rstudio.com/products/RStudio/) sur votre ordinateur personnel , de préférence avant la première séance. 
    "Il s’agit de deux logiciels libres, gratuits, téléchargeables en ligne et fonctionnant sous Windows, Mac-OS et Linux.

   
Pour installer R:
    
   * [Installer R sous Windows](https://cloud.r-project.org/bin/windows/base)
    
   * [Installer R sous Mac](https://cloud.r-project.org/bin/macosx)

   * Sous Linux R n’est fourni que comme un outil en ligne de commande, utilisez votre gestionnaire de packages habituel.

    
[RStudio](https://www.rstudio.com/products/RStudio/) est un environnement de développement intégré, qui propose des outils et facilite 
l’écriture de scripts et l’usage de R au quotidien. C’est une interface bien supérieure à celles fournies par défaut lorsqu’on 
installe R sous Windows ou sous Mac-OS
   
   [Installer RStudio](https://www.rstudio.com/products/rstudio/download/#download), téléchargez la version adaptée à votre système :
    
Nous verrons comment installer de nouveaux packages.
