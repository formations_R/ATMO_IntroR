

```R
setwd("/home/viryl/notebooks/ATMO_IntroR")
```

# Manipuler des données dans R

En statistique, les données constituent le point de départ de toute analyse, un premier travail de mise en forme de ces données est presque toujours indispensable. Il faudra savoir maitriser des opérations comme:

* Importation de données sous différents formats, 
* exporter des données et des résultats sous différents formats,
* concaténer ou extraire des données, 
* repérer les individus ayant des données manquantes ou aberrantes.
* changer le type de certaines variables pour les adapter aux traitements envisagés,
* $\ldots$

R fournit des outils et des capacités de programmation pour effectuer ces différentes tâches.

## Importer des données

Les données sont initialement collectées, stockées sous différents formats, éventuellement prétraitées par un logiciel ou extraites d'une base de données. 

Chaque logiciel ayant son propre format de stockage, le plus simple est souvent d'échanger les données par un format commun à tous qui sera le plus souvent **le format texte** ( .csv par exemple).

On peut également utiliser **les formats propriétaires** (SAS,SPSS,...) des autres logiciels en utilisant un package adapté (le package **foreign** par exemple), le choix dépendant du contexte et du volume des données.

## Importer des données en format texte
### Cas des fichiers **csv** 

Les avantages des fichiers **csv**:

  - Peut être lu par n'importe quel logiciel passé, présent et probablement futur.
  - Pour la compatibilité entre plate-forme (Windows, Mac, Linux).
  - Pour la facilité de lecture par un être humain comparativement à d'autres formats tels que XML, HL7, JSON etc.

**Mais** pas forcément adapté aux gros volumes de données pour son volume de stockage et la rapidité de lecture.

R lit des données en format texte avec les fonctions **read.table()**,**read.csv()**,**scan()**,**read.fwf()**,$\ldots$

* Le fichier "donnees.csv" est stocké dans le répertoire data sitée dans le répertoire de travail 
    
    - pour connaître le répertoire de travail, utiliser la fonction **getwd()**
    - pour définir le répertoire de travail, la fonction **setwd()**
    
 Le résultat de la fonction **read.table** ou **read.csv** est de type **data-frame**.


```R
# Lecture du fichier donnees.csv
getwd() # repertoire de travail
don <- read.csv(file = "data/donnees.csv",header=TRUE,sep=";",dec=",",row.names=1)
```

   - l'argument **sep** : indique que les valeurs sont séparées par **";"** (**" "** pour un espace, **"\t"** pour une tabulation,$\ldots$)
   - l'argument **dec** : indique que le séparateur de décimal est **","**
   - l'argument **header** : indique si la première ligne contient les noms des variables (TRUE) ou non(FALSE).
   - l'argument **row.names** : indique que la colonne 1 n'est pas une variable mais l'identifiant des individus.
   


```R
# "mode" et "class" 
mode(don)
class(don)
```


'list'



'data.frame'



```R
# Statistiques des variables du data.frame
summary(don)
```


         taille          poids          pointure    sexe 
     Min.   :158.0   Min.   : 5.00   Min.   :42.0   M:3  
     1st Qu.:166.8   1st Qu.:38.50   1st Qu.:42.5        
     Median :175.5   Median :72.00   Median :43.0        
     Mean   :172.5   Mean   :52.33   Mean   :43.0        
     3rd Qu.:179.8   3rd Qu.:76.00   3rd Qu.:43.5        
     Max.   :184.0   Max.   :80.00   Max.   :44.0        


#### Fonctions utilisees sur un data-frame


```R
# Attributs d'un data-frame
attributes(don)
```


<dl>
	<dt>$names</dt>
		<dd><ol class=list-inline>
	<li>'taille'</li>
	<li>'poids'</li>
	<li>'pointure'</li>
	<li>'sexe'</li>
</ol>
</dd>
	<dt>$class</dt>
		<dd>'data.frame'</dd>
	<dt>$row.names</dt>
		<dd><ol class=list-inline>
	<li>'roger'</li>
	<li>'theodule'</li>
	<li>'nicolas'</li>
</ol>
</dd>
</dl>




```R
# Afficher de manière compacte la structure d'un objet R 
str(don)
```

    'data.frame':	3 obs. of  4 variables:
     $ taille  : num  184 176 158
     $ poids   : int  80 5 72
     $ pointure: int  44 43 42
     $ sexe    : Factor w/ 1 level "M": 1 1 1



```R
# Nom des variables 
names(don)
```


<ol class=list-inline>
	<li>'taille'</li>
	<li>'poids'</li>
	<li>'pointure'</li>
	<li>'sexe'</li>
</ol>




```R
# Nombre de lignes(individus) et de colonnes(variables)
nrow(don)
ncol(don)
```


3



4



```R
# Nom des lignes(1.) et des colonnes(2.)
dimnames(don)
```


<ol>
	<li><ol class=list-inline>
	<li>'roger'</li>
	<li>'theodule'</li>
	<li>'nicolas'</li>
</ol>
</li>
	<li><ol class=list-inline>
	<li>'taille'</li>
	<li>'poids'</li>
	<li>'pointure'</li>
	<li>'sexe'</li>
</ol>
</li>
</ol>



#### Un caractère spécial peut indiquer qu'il y a des données manquantes:

Le fichier **don2.csv** contient des données manquantes codées **"\*\*\*"**, on ajoute l'argument **na.strings**


```R
don2 <- read.csv(file = "data/don2.csv",header=TRUE,sep=";",dec=",",row.names=1,na.strings="***")
summary(don2)
mean(don2$poids)
mean(don2$poids,na.rm=TRUE)
```


         taille          poids       pointure       sexe  
     Min.   :100.0   Min.   :15   Min.   :22.00   F   :2  
     1st Qu.:110.0   1st Qu.:30   1st Qu.:30.50   M   :3  
     Median :160.0   Median :72   Median :40.00   NA's:1  
     Mean   :145.9   Mean   :55   Mean   :36.17           
     3rd Qu.:175.5   3rd Qu.:78   3rd Qu.:42.75           
     Max.   :184.0   Max.   :80   Max.   :44.00           
     NA's   :1       NA's   :1                            



    [1] NA



55


#### Le chemin peut-être une URL:


```R
df <- read.table("https://s3.amazonaws.com/assets.datacamp.com/blog_assets/scores_timed.csv",header=TRUE,row.names = 1,sep = ",")
summary(df)
```


         SCORE            TIME    DECIMAL.TIME            CLASS  
     Min.   : 6.0   03:22:50:1   Min.   :0.140   BEST        :2  
     1st Qu.: 8.0   07:42:51:1   1st Qu.:0.320   INTERMEDIATE:1  
     Median :13.0   09:30:03:1   Median :0.400   WORST       :2  
     Mean   :12.4   12:01:03:1   Mean   :0.372                   
     3rd Qu.:16.0   12:01:29:1   3rd Qu.:0.500                   
     Max.   :19.0                Max.   :0.500                   


### La fonction **scan**

La fonction **scan** est plus flexible que **read.table**.

* Une différence est qu'il est possible de spécifier le mode des variables:


```R
mydata <- scan("data/donnees.csv",skip=1,sep=";",dec=",",what = list("", 0, 0,0,""))
class(mydata)
str(mydata)
mydata[[1]] # premiere variable
mydata[[1]][1] # 
```


'list'


    List of 5
     $ : chr [1:3] "roger" "theodule" "nicolas"
     $ : num [1:3] 184 176 158
     $ : num [1:3] 80 5 72
     $ : num [1:3] 44 43 42
     $ : chr [1:3] "M" "M" "M"



<ol class=list-inline>
	<li>'roger'</li>
	<li>'theodule'</li>
	<li>'nicolas'</li>
</ol>




'roger'


Dans cet exemple, **scan** lit *5 variables*, la première en mode caractère, les trois suivantes sont en mode numérique et la cinquième en mode caractère.

**mydata** est une liste de 5 vecteurs.

* scan() peut être utilisée pour créer des objets de mode différent (vecteurs, matrices, tableaux de données, listes,...).
*
* Par défaut, c'est-à-dire si what est omis, scan() crée un vecteur numérique.

Pour en savoir plus **help(scan)**

## Importer des fichiers Excel
Le package **readxl** est l'outil le plus simple pour importer des fichiers *Excel* au format **xls** ou **xlsx**, il n'a pas de dépendances externes et il est facile à installer sur tout système. Il existe d'autres packages comme *gdata, xlsx, xlsReadWrite*...

Le package **readxl** fait partie du package **tidyverse** mais il peut être installé séparément et devra être chargé séparément.

On utilisera le fichier Excel "datasets.xls" (ou "datasets.xlsx") fournit par le package *readxl*.

### Lire le fichier excel:
On utilise la fonction **read_excel**, l'objet fournit est de type "*data.frame*,*tbl_df*,*tbl*". La fonction lit à la fois les fichiers de type **xls et xlsx**, il détermine le format à partir de l'extension.


```R
# Importer le package
library("readxl")
```


```R
# Path du fichier excel "datasets.xls"
datasets <- readxl_example("datasets.xls") # chemin de stockage du fichier excel "datasets.xls"
datasets
# Lecture du fichier excel "datasets.xls"
df <- read_excel(datasets)
```


'/opt/conda/lib/R/library/readxl/extdata/datasets.xls'



```R
# "class" de l'objet df
class(df)
```


```R
summary(df)
```


<ol class=list-inline>
	<li>'tbl_df'</li>
	<li>'tbl'</li>
	<li>'data.frame'</li>
</ol>




      Sepal.Length    Sepal.Width     Petal.Length    Petal.Width   
     Min.   :4.300   Min.   :2.000   Min.   :1.000   Min.   :0.100  
     1st Qu.:5.100   1st Qu.:2.800   1st Qu.:1.600   1st Qu.:0.300  
     Median :5.800   Median :3.000   Median :4.350   Median :1.300  
     Mean   :5.843   Mean   :3.057   Mean   :3.758   Mean   :1.199  
     3rd Qu.:6.400   3rd Qu.:3.300   3rd Qu.:5.100   3rd Qu.:1.800  
     Max.   :7.900   Max.   :4.400   Max.   :6.900   Max.   :2.500  
       Species         
     Length:150        
     Class :character  
     Mode  :character  
                       
                       
                       


Il est possible de spécifier la feuille et la plage de cellules que l’on souhaite importer avec les arguments **sheet** et **range**.
### Gestion des feuilles d'un classeur Excel
Un **classeur Excel** peut contenir plusieurs feuilles. 


```R
# Liste des feuilles du classeur "datasets.xls".
excel_sheets(datasets)
```


<ol class=list-inline>
	<li>'iris'</li>
	<li>'mtcars'</li>
	<li>'chickwts'</li>
	<li>'quakes'</li>
</ol>




```R
# Lecture d'une des feuilles
df <- read_excel(datasets,sheet = "iris")
class(df)
summary(df)
```


<ol class=list-inline>
	<li>'tbl_df'</li>
	<li>'tbl'</li>
	<li>'data.frame'</li>
</ol>




      Sepal.Length    Sepal.Width     Petal.Length    Petal.Width   
     Min.   :4.300   Min.   :2.000   Min.   :1.000   Min.   :0.100  
     1st Qu.:5.100   1st Qu.:2.800   1st Qu.:1.600   1st Qu.:0.300  
     Median :5.800   Median :3.000   Median :4.350   Median :1.300  
     Mean   :5.843   Mean   :3.057   Mean   :3.758   Mean   :1.199  
     3rd Qu.:6.400   3rd Qu.:3.300   3rd Qu.:5.100   3rd Qu.:1.800  
     Max.   :7.900   Max.   :4.400   Max.   :6.900   Max.   :2.500  
       Species         
     Length:150        
     Class :character  
     Mode  :character  
                       
                       
                       


*Remarque*: par défaut la fonction read_excel lit la première feuille du fichier Excel fournit en argument.<br>

### Plusieurs façons de spécifier quelles cellules sont lues.
#### Une partie des lignes du tableau de données


```R
read_excel(datasets,sheet = "iris",n_max=3)
```


<table>
<thead><tr><th scope=col>Sepal.Length</th><th scope=col>Sepal.Width</th><th scope=col>Petal.Length</th><th scope=col>Petal.Width</th><th scope=col>Species</th></tr></thead>
<tbody>
	<tr><td>5.1   </td><td>3.5   </td><td>1.4   </td><td>0.2   </td><td>setosa</td></tr>
	<tr><td>4.9   </td><td>3.0   </td><td>1.4   </td><td>0.2   </td><td>setosa</td></tr>
	<tr><td>4.7   </td><td>3.2   </td><td>1.3   </td><td>0.2   </td><td>setosa</td></tr>
</tbody>
</table>




```R
read_excel(datasets,sheet = "iris",range = cell_rows(1:4))
```


<table>
<thead><tr><th scope=col>Sepal.Length</th><th scope=col>Sepal.Width</th><th scope=col>Petal.Length</th><th scope=col>Petal.Width</th><th scope=col>Species</th></tr></thead>
<tbody>
	<tr><td>5.1   </td><td>3.5   </td><td>1.4   </td><td>0.2   </td><td>setosa</td></tr>
	<tr><td>4.9   </td><td>3.0   </td><td>1.4   </td><td>0.2   </td><td>setosa</td></tr>
	<tr><td>4.7   </td><td>3.2   </td><td>1.3   </td><td>0.2   </td><td>setosa</td></tr>
</tbody>
</table>




```R
read_excel(datasets,sheet = "iris",range = cell_rows(1:3))
```


<table>
<thead><tr><th scope=col>Sepal.Length</th><th scope=col>Sepal.Width</th><th scope=col>Petal.Length</th><th scope=col>Petal.Width</th><th scope=col>Species</th></tr></thead>
<tbody>
	<tr><td>5.1   </td><td>3.5   </td><td>1.4   </td><td>0.2   </td><td>setosa</td></tr>
	<tr><td>4.9   </td><td>3.0   </td><td>1.4   </td><td>0.2   </td><td>setosa</td></tr>
</tbody>
</table>



#### Une partie des colonnes


```R
read_excel(datasets,sheet = "iris",range = cell_cols("B:D"))
```


<table>
<thead><tr><th scope=col>Sepal.Width</th><th scope=col>Petal.Length</th><th scope=col>Petal.Width</th></tr></thead>
<tbody>
	<tr><td>3.5</td><td>1.4</td><td>0.2</td></tr>
	<tr><td>3.0</td><td>1.4</td><td>0.2</td></tr>
	<tr><td>3.2</td><td>1.3</td><td>0.2</td></tr>
	<tr><td>3.1</td><td>1.5</td><td>0.2</td></tr>
	<tr><td>3.6</td><td>1.4</td><td>0.2</td></tr>
	<tr><td>3.9</td><td>1.7</td><td>0.4</td></tr>
	<tr><td>3.4</td><td>1.4</td><td>0.3</td></tr>
	<tr><td>3.4</td><td>1.5</td><td>0.2</td></tr>
	<tr><td>2.9</td><td>1.4</td><td>0.2</td></tr>
	<tr><td>3.1</td><td>1.5</td><td>0.1</td></tr>
	<tr><td>3.7</td><td>1.5</td><td>0.2</td></tr>
	<tr><td>3.4</td><td>1.6</td><td>0.2</td></tr>
	<tr><td>3.0</td><td>1.4</td><td>0.1</td></tr>
	<tr><td>3.0</td><td>1.1</td><td>0.1</td></tr>
	<tr><td>4.0</td><td>1.2</td><td>0.2</td></tr>
	<tr><td>4.4</td><td>1.5</td><td>0.4</td></tr>
	<tr><td>3.9</td><td>1.3</td><td>0.4</td></tr>
	<tr><td>3.5</td><td>1.4</td><td>0.3</td></tr>
	<tr><td>3.8</td><td>1.7</td><td>0.3</td></tr>
	<tr><td>3.8</td><td>1.5</td><td>0.3</td></tr>
	<tr><td>3.4</td><td>1.7</td><td>0.2</td></tr>
	<tr><td>3.7</td><td>1.5</td><td>0.4</td></tr>
	<tr><td>3.6</td><td>1.0</td><td>0.2</td></tr>
	<tr><td>3.3</td><td>1.7</td><td>0.5</td></tr>
	<tr><td>3.4</td><td>1.9</td><td>0.2</td></tr>
	<tr><td>3.0</td><td>1.6</td><td>0.2</td></tr>
	<tr><td>3.4</td><td>1.6</td><td>0.4</td></tr>
	<tr><td>3.5</td><td>1.5</td><td>0.2</td></tr>
	<tr><td>3.4</td><td>1.4</td><td>0.2</td></tr>
	<tr><td>3.2</td><td>1.6</td><td>0.2</td></tr>
	<tr><td>⋮</td><td>⋮</td><td>⋮</td></tr>
	<tr><td>3.2</td><td>5.7</td><td>2.3</td></tr>
	<tr><td>2.8</td><td>4.9</td><td>2.0</td></tr>
	<tr><td>2.8</td><td>6.7</td><td>2.0</td></tr>
	<tr><td>2.7</td><td>4.9</td><td>1.8</td></tr>
	<tr><td>3.3</td><td>5.7</td><td>2.1</td></tr>
	<tr><td>3.2</td><td>6.0</td><td>1.8</td></tr>
	<tr><td>2.8</td><td>4.8</td><td>1.8</td></tr>
	<tr><td>3.0</td><td>4.9</td><td>1.8</td></tr>
	<tr><td>2.8</td><td>5.6</td><td>2.1</td></tr>
	<tr><td>3.0</td><td>5.8</td><td>1.6</td></tr>
	<tr><td>2.8</td><td>6.1</td><td>1.9</td></tr>
	<tr><td>3.8</td><td>6.4</td><td>2.0</td></tr>
	<tr><td>2.8</td><td>5.6</td><td>2.2</td></tr>
	<tr><td>2.8</td><td>5.1</td><td>1.5</td></tr>
	<tr><td>2.6</td><td>5.6</td><td>1.4</td></tr>
	<tr><td>3.0</td><td>6.1</td><td>2.3</td></tr>
	<tr><td>3.4</td><td>5.6</td><td>2.4</td></tr>
	<tr><td>3.1</td><td>5.5</td><td>1.8</td></tr>
	<tr><td>3.0</td><td>4.8</td><td>1.8</td></tr>
	<tr><td>3.1</td><td>5.4</td><td>2.1</td></tr>
	<tr><td>3.1</td><td>5.6</td><td>2.4</td></tr>
	<tr><td>3.1</td><td>5.1</td><td>2.3</td></tr>
	<tr><td>2.7</td><td>5.1</td><td>1.9</td></tr>
	<tr><td>3.2</td><td>5.9</td><td>2.3</td></tr>
	<tr><td>3.3</td><td>5.7</td><td>2.5</td></tr>
	<tr><td>3.0</td><td>5.2</td><td>2.3</td></tr>
	<tr><td>2.5</td><td>5.0</td><td>1.9</td></tr>
	<tr><td>3.0</td><td>5.2</td><td>2.0</td></tr>
	<tr><td>3.4</td><td>5.4</td><td>2.3</td></tr>
	<tr><td>3.0</td><td>5.1</td><td>1.8</td></tr>
</tbody>
</table>



**ou**


```R
read_excel(datasets,sheet = "iris",range = cell_cols(2:4))
```

#### Une partie des lignes et une partie des colonnes 


```R
read_excel(datasets,sheet = "mtcars",range = "B1:D5")
```


<table>
<thead><tr><th scope=col>cyl</th><th scope=col>disp</th><th scope=col>hp</th></tr></thead>
<tbody>
	<tr><td>6  </td><td>160</td><td>110</td></tr>
	<tr><td>6  </td><td>160</td><td>110</td></tr>
	<tr><td>4  </td><td>108</td><td> 93</td></tr>
	<tr><td>6  </td><td>258</td><td>110</td></tr>
</tbody>
</table>



**ou**


```R
read_excel(datasets,range ="mtcars!B1:D5")
```


<table>
<thead><tr><th scope=col>cyl</th><th scope=col>disp</th><th scope=col>hp</th></tr></thead>
<tbody>
	<tr><td>6  </td><td>160</td><td>110</td></tr>
	<tr><td>6  </td><td>160</td><td>110</td></tr>
	<tr><td>4  </td><td>108</td><td> 93</td></tr>
	<tr><td>6  </td><td>258</td><td>110</td></tr>
</tbody>
</table>



#### "Not Available" / Missing Values représenté par une chaîne de caractères


```R
df <-read_excel(datasets,sheet = "iris",range="B1:E5",na="1.4")
```


```R
summary(df)
mean(df$Petal.Length)
mean(df$Petal.Length,na.rm=TRUE)
```


      Sepal.Width     Petal.Length   Petal.Width    Species         
     Min.   :3.000   Min.   :1.30   Min.   :0.2   Length:4          
     1st Qu.:3.075   1st Qu.:1.35   1st Qu.:0.2   Class :character  
     Median :3.150   Median :1.40   Median :0.2   Mode  :character  
     Mean   :3.200   Mean   :1.40   Mean   :0.2                     
     3rd Qu.:3.275   3rd Qu.:1.45   3rd Qu.:0.2                     
     Max.   :3.500   Max.   :1.50   Max.   :0.2                     
                     NA's   :2                                      



    [1] NA



1.4


### Quelques références
[Article "readxl"](https://readxl.tidyverse.org/articles/index.html) <br>
https://readxl.tidyverse.org/<br>
[Cookbook for R](http://www.cookbook-r.com/Manipulating_data/)<br>
...

## Importer des fichiers avec un format propriétaire

R peut également lire des fichiers dans d'autres formats (**Excel, SAS, SPSS**,$\ldots$) et accéder à des **bases de données**.

* Le package **foreign** permet d'importer des données en format propriétaire binaire tels que **Stata, SAS, SPSS, etc**.

  **library**(foreign) <br\>
  **read.dta**("calf pneu.dta") # for Stata files<br\>
  **read.xport**("file.xpt") # for SAS XPORT format<br\>
  **read.spss**("file.sav") # for SPSS format<br\>
  **read.mpt**("file.mtp") # for Minitab Portable Worksheet<br\>

Une autre solution pour des **fichiers SPSS**, le package **Hmisc**<br\>

## Base de données relationnelles et autres formats

Certains packages permettent de se connecter directement sur des bases de données de type **MySQL** ou **PostgreSQL**
(MongoDB,Redis,ousqlite,...). Dans ce cas,le mode d’interaction avec les données est légèrement différent
 car on utilise alors le langage de requête propre au langage, à moins d’utiliser des packages qui permettent d’assurer
 la conversion à partir des commandes R habituelles telles que **subset()**.

Pour en savoir plus : 

* [Cookbook for R](http://www.cookbook-r.com/Manipulating_data/)

## Sauvegarder des données ou des résultats

Une fois les analyses effectuées et les résultats obtenus, il est souvent important de les sauvegarder pour les communiquer à d'autres personnes ou d'autres logiciels ou les réutiliser dans d'autres analyses.

### En format texte

Le format texte est utilisé, il sera compatible avec d'autres logiciels, on utilise la fonction **write.table()**.


```R
tablo_res <- matrix(runif(40), ncol=4)
# sauvegarde les resulatts dans le repertoire resultat
setwd("/home/viryl/notebooks/ATMO-IntroR/resultat")
write.table(tablo_res,"monfichier.csv",sep=";",row.names=FALSE)


```


    Error in setwd("/home/viryl/notebooks/ATMO-IntroR/resultat"): cannot change working directory
    Traceback:


    1. setwd("/home/viryl/notebooks/ATMO-IntroR/resultat")


### En format binaire RData

* Exporter les résultats et/ou les données dans un fichier binaire suffixé
**.Rdata** que R sera capable de décrypter par la suite grâce à la fonction **save()**

   **save(file="nom du fichier",liste des variables)** (help(save))
   


```R
x <- runif(20)
y <- list(a = 1, b = TRUE, c = "oops")
getwd()
save(x, y, file = "xy.RData")
```

* L'utilisation des fichiers sauvegardées par la commande **save()** se fait par la commande **load()**.


```R
rm(list=ls())
"avant load"
ls()
load(file="xy.RData")
"après load"
ls()
```

## Manipuler des variables

* **Changer le type d'une variable**: à l'issue d'une importation, une variable qualitative dont les modalitées sont numériques sera comprise par R comme une variable quantitative,$\ldots$

* **Découper en classes une variable quantitative**: le passage d'une variable quantitative à une variable qualitative est fréquemment nécessaire en statistiques pour l'adapter à la méthode utilisée (AFC,AFCM,$\ldots$)

* **Modifier les niveaux d'une variable qualitative**: fusionner un ou plusieurs niveaux en fonction des effectifs,$\ldots$

* **Repérer les données manquantes**: permettre la prise en compte des données manquantes dans le traitement statistique des données.

* **Repérer les données aberrantes**: permettre la prise en compte des données aberrantes dans le traitement statistique des données.

* $\ldots$

### Quantitatives ou qualitative?

 Trois méthodes:
 


```R
# on genere une variable X
X <-c(rep(5,2),rep(12,4),13)
X
# Une variable qualitative?
Xq <is.factor(X)
# Une variable quantitative
is.numeric(X)
# Statistques descriptives
summary(X)
```

Les trois instructions nous indiquent que X est une variable quantitative.

### Variable qualitative vers variable quantitative
* Avec recodification des modalités: 1 seule étape

Par défaut, la conversion d'un facteur en variable quantitative, utilise la recodification des modalités à **1 to nlevels(x)**


```R
Xqual <- factor(c(rep(5,2),seq(0,6,by=2),13))
Xqual
nlevels(Xqual)
summary(Xqual)
is.factor(Xqual)
as.numeric(Xqual)
```

* Sans recodification des modalités

Si on veut éviter la recofication, lorsque la valeur des modalités a un sens de quantité, il sera nécessaire de faire une première conversion de mode en mode **"character"**, avant la conversion en **"numeric"**.


```R
Xqual
prov <- as.character(Xqual)
prov
Xquant <- as.numeric(prov) # as.numeric(as.character(Xqual))
Xquant
summary(Xquant)
```

### Variable quantitative vers variable qualitative
* En conservant les valeurs de la variable quantitative


```R
rm(list=ls())
ls()
X <- seq(0,10,2)
X
summary(X)
Xqual <- as.factor(X)
Xqual
summary(Xqual)
```

### Découpage en classes d'une variable quantitative

Le découpage en classes d'une variable quantitatives peut se faire avec **deux approches**:

* Les **seuils des classes** sont choisis par les utilisateurs: pour définir ces seuils de façon automatique, on utilisera la fonction **cut**. Les bornes des intervals sont fournies à la fonction cut, les classes sont de la forme $\bf{] a_i,a_{i+1}]}$.


```R
set.seed(654) # on fixe la graine du generateur
X <- rnorm(n=100,mean=0,sd=1)
# Decoupage en 3 niveaux: [min(X),-0.2],[-0.2,0.2],[0.2,max(X)]
Xqual <- cut(X,breaks = c(min(X)-1e-10,-0.2,0.2,max(X)))
class(Xqual)
table(Xqual)
summary(Xqual) # ou table(Xqual)
```

On indique "$min(X)-1e-10$" pour que le minimum appartienne à la classe.

* Un découpage automatique proposant **des effectifs équivalents dans chaque classes**: si nous voulons des effectifs équilibrés dans chacune des trois modalités, on utilisera la **fonction quantile**.


```R
decoupe <- quantile(X,probs=seq(0,1,length=4)) 
decoupe
decoupe[1] <- decoupe[1]-1e-10
Xqual <- cut(X,decoupe)
table(Xqual)
```

### Modifier les modalités d'un facteur

* Modifier les labels des modalités


```R
Xqual <- cut(X,decoupe)
levels(Xqual)
#Xqual
table(Xqual)
#
levels(Xqual) <- c(1,2,3) # modification les labels des modalités
#Xqual
table(Xqual)
```

* Fusionner un ou plusieurs niveaux


```R
levels(Xqual)
levels(Xqual) <- c(1,2,1)
levels(Xqual)
table(Xqual)
```

* Définir un niveau de référence: pour certaines méthodes, il faudra tenir compte de l'ordre d'apparition des niveaux ou spécifier une niveau de référence (analyse de variance,$\ldots$), on utilise la fonction **relevel**:


```R
X <- c(1,2,1,3,2,2,1)
Xqual <- factor(X,label=c("classique","nouveau","placebo"))
levels(Xqual)
Xqual2 <- relevel(Xqual,ref="placebo")
levels(Xqual2)
```

* Contrôler l'ordre des niveaux: recréer un facteur à partir du facteur existant en spécifiant l'ordre des niveaux.


```R
table(Xqual)
Xqual3 <- factor(Xqual,levels=c("placebo", "nouveau","classique"))
Xqual3 <- Xqual3[-4] # elimine l'individu avec la modalite "3"
#Xqual3
table(Xqual3) # la modalite "3" n'apparait plus
# elimine la modalite "3"
Xqual3 <- factor(as.character(Xqual3)) # elimine la modalite "3"
table(Xqual3)
```

## Manipuler des individus

### Repérer les individus manquants 
Dans R, les données manquantes sont représentées par **NA**. La fonction **is.na**
permet de les retrouver.

* Dans une variable:


```R
X <- rnorm(10,0,1)
X[c(2,7,10)] <- NA
summary(X)
mean(X)
mean(X,na.rm=TRUE)
#
selectNA <- is.na(X)
selectNA
which(selectNA) # Quels sont les indices correspondants
# "!" : negation 
X2 <-X[!selectNA] # On élimine les individus correspondants
```

* Dans sun tableau de données:


```R
Y <- factor(c(rep("A",3),NA,rep("M",4),"D",NA))
# data-frame avec les deux variables X et Y
don <-data.frame(X,Y)
summary(don)
# 
selectNA <- is.na(don)
#mode(selectNA)
class(selectNA)
#selectNA
#
# au moins une donnee manquante pour un individu
aelim_any <- apply(selectNA, MARGIN=1,FUN=any)
aelim_any 
# Toutes les donnees sont manquantes pour un individu
aelim_all <- apply(selectNA, MARGIN=1,FUN=all)
aelim_all
```


```R
don2 <-don[!aelim_all,] # individus élimines
which(is.na(don))
which(is.na(don),arr.ind=T) # option arr.ind (array indices) de which
```

### Repérer les individus aberrants

On utilise la fonction boxplot qui fournit dans sa composante out les valeurs
aberrantes.

Dans l'exemple, on utilise le package "rpart", vous pouvez l'installer en local, voir ~/Utils/install_local_package.ipynb


```R
rm(list=ls())
```


```R
library(rpart)
data("kyphosis")
names(kyphosis)
boxNumber <- boxplot(kyphosis[,"Number"]) # repere
```


```R
class(boxNumber)
attributes(boxNumber)
```


```R
# les individus aberrants
valaberrante <- boxNumber$out
#kyphosis[,"Number"]%in%valaberrante
which(kyphosis[,"Number"]%in%valaberrante)
```

## Concaténer des tableaux de données

Pour regrouper deux tableaux peut être vu de deux façons:

* Aggréger des individus sur lesquels ont été observées les mêmes variables en concaténant des tableaux de données l'un en dessous de l'autre avec la fonction **rbind**.


```R
# exemple rbind sur des matrices avec le même nombre de collonnes
X <- matrix(21:24,ncol=2)
colnames(X) <- paste("X",1:2,sep="")
rownames(X) <- paste("individu",1:2,sep="")
X
Y <- matrix(11:14,ncol=2)
colnames(Y) <- paste("Y",1:2,sep="")
rownames(Y) <- paste("individu",3:4,sep="")
Y
Z <-rbind(X,Y)
Z
class(Z)
```

Les deux matrices **X**et **Y** n'ont pas les mêmes noms de collonne, la matrice issue de la concaténation prend les noms de la première matrice.

* pour la concaténation des data-frames, il sera nécessaire que les deux data-frame aient les mêmes noms de variables. dans la cas contraire, il sera nécessaire de renommer les variables d'un data-frame.


```R
Xd <- data.frame(X)
Yd <- data.frame(Y)
Zd <- rbind(Xd,Yd)
```


```R
colnames(Yd) <- colnames(Xd)
Zd <- rbind(Xd,Yd)
Zd
class(Zd)
```

* Aggréger des variables qui ont été observées sur les même individus en concaténant des tableaux de données l'un à coté de l'autre avec la fonction **cbind()**.


```R
Xd <- data.frame(matrix(21:24,ncol=2))
colnames(Xd) <- paste("X",1:2,sep="")
rownames(Xd) <- paste("individu",1:2,sep="")
Xd
Yd <- data.frame(matrix(11:14,ncol=2))
colnames(Yd) <- paste("Y",1:2,sep="")
rownames(Yd) <- paste("individu",3:4,sep="")
Yd
Wd <-cbind(Xd,Yd)
Wd

```

La fonction **cbind()** ne vérifie pas le nom des lignes, les noms des lignes du premier data-frame sont conservés. 

* Il est possible de fusionner deux tableaux selon une **clef** avec la fonction **merge**.

Concaténons deux tableaux de données:
  - le premier tableau regroupe une variable continue (age) et deux variables qualitatives ("prenom" , "ville").


```R
age <- c(45,32,67)
ville <- factor(c("rennes","rennes","marseille"))
prenom <- c("Alice","Marcel","Alexis")
indiv <- cbind.data.frame(age, prenom, ville)
class(indiv)
indiv
```

  - le second tableau regroupe les caractéristiques des villes.


```R
population <- c(300,500,600)
caractVille <- cbind.data.frame(ville =c("rennes","lyon","marseille"),pop=population)
caractVille
```

On fusionne les tableaux en un seul où seront répétées les caractéristiques des villes à chaque ligne du tableau. On effectue une fusion avec la fonction **merge()** et la clef **ville**.


```R
merge(indiv,caractVille,by="ville")
```

*exercices*: ~/CED-IntroR/TP/enonces/ImportFusion.pdf

## Tableaux croisés

Lorsqu'on a deux variables qualitatives observées sur un échantillon, les données
peuvent être présentées sous deux formes:

### Tableau de contingences


```R
tension <- factor(c(rep("Faible",5),rep("Forte",5)))
tension
laine <- factor(c(rep("Mer",3),rep("Ang",3),rep("Tex",4)))
laine
# fusionnons ces deux variables dans un data.frame
don <-data.frame(tension, laine) # cbind.data.frame(tension,laine)
# Tableau de contingences
tabcroise <-table(don$tension,don$laine)
class(tabcroise)
tabcroise
```

### Tableaux Individus X Variables


```R
tabframe <- as.data.frame(tabcroise)
tabframe
```

Nous obtenons une fréquence pour chaque combinaison et non pas une
ligne par individu. (pas très compliqué à obtenir, voir exercice indVarTab.ipynb)

Pour en savoir plus:

* [Gestion des données avec R](https://www.fun-mooc.fr/c4x/UPSUD/42001S03/asset/data-management.html) (Christophe Lalanne & Bruno Falissard -MOOC "Introduction à la statistique avec R").

* [Begin'R (Bordeaux INP](http://beginr.u-bordeaux.fr/index.html#sommaire))

* [Cookbook for R](http://www.cookbook-r.com/Manipulating_data/)

* [Introduction à R et au tidyverse](https://juba.github.io/tidyverse/index.html)
