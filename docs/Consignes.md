Pour chaque partie de ce cours, vous aurez des fichiers notebooks que vous pourrez utiliser sur le [serveur de notebook de GRICAD](https://jupyterhub.u-ga.fr/hub/login?next=%2Fhub%2Fuser%2Fviryl%2Ftree%3F), 
une version statique du cours, des scripts, des données pour les TPs...

[Pour en savoir plus sur les notebooks (jupyter, Rmarkdown)](https://www.datacamp.com/community/blog/jupyter-notebook-r#gs.6iMBYHw)

  **L'ensemble de ces fichiers seront récupérés sur le serveur Gitlab de GRICAD** (projet [ATMO-IntroR](https://gricad-gitlab.univ-grenoble-alpes.fr/formations_R/ATMO_IntroR)).

https://gricad-gitlab.univ-grenoble-alpes.fr/formations-statistiques-R/ATMO_IntroR)).

  **L'accès aux deux plate-formes se fait à l'aide de votre compte Agalan**

## La plate-forme gitlab de l'UMS GRICAD

   [gricad-gitlab](https://gricad-gitlab.univ-grenoble-alpes.fr/) est une plate-forme de travail collaboratif destinée à l'ensemble de 
   la communauté enseignement-recherche grenobloise. Elle est hébergée et administrée par l'UMS GRICAD. 
    
Ce cours est associé au projet [ATMO_IntroR](https://gricad-gitlab.univ-grenoble-alpes.fr/formations_R/ATMO_IntroR.git) de cette plate-forme.
   
   - pour récupérer le matériel du cours, exécuter dans un terminal, dans un répertoire de travail à votre convenance, la commande:
    
      **git clone https://gricad-gitlab.univ-grenoble-alpes.fr/formations-statistiques-R/ATMO_IntroR.git/**
      
      
   - ou exécuter sur le serveur [jupyter-GRICAD](https://jupyterhub.u-ga.fr/) le notebook **IntroR-config.ipynb** qui vous sera fourni.
   
   Vous récupérez un répertoire ATMO-IntroR dans lequel figurent tous les fichiers qui seront utilisés (notebooks, données, scripts R,...).
    
## Le serveur de notebooks de l'UMS GRICAD
    
 - Connectez vous sur [le  erveur de notebook de GRICAD](https://jupyterhub.u-ga.fr/) avec votre compte Agalan.
  
 - Exécutez le notebook **Intro_config.ipynb** dans le répertoire **~/ATMO_IntroR/notebook** du serveur. C'est la page d'accueil, à la connexion sur le serveur.
 
 - Exemple d'utilisation, suivre le cours "Introduction", première partie de ce cours.
 
 - Télécharger le fichier **IntroR.ipynb** figurant dans le répertoire **~/ATMO_IntroR/notebooks** que vous avez récupéré sur le gitlab de GRICAD (bouton Upload en haut à droite de l'écran).
 
 - Vous avez dans ce documents deux types de cellules, des cellules de type " Markdown" et des cellules de code R, pour interpréter ces cellules faire **shift-return** simultanément.
 
 - Pour en savoir plus jupyter, regarder le tutoriel "ipython-in-depth".

## Surles postes de travail de la salle de formation ou votre portable

Pour travailler avec des notebooks, utiliser **jupyter** de python: <br>

jupyter notebook <nom du fichier notebook>

# Si vous voulez travailler sur votre propre station ou portable
## Installation de R sur votre portable

Pour un meilleur suivi du cours, nous vous conseillons d'installer R (Version 3.4.x au moins) et [RStudio](https://www.rstudio.com/products/RStudio/) sur votre ordinateur personnel, de préférence avant la première séance. 
    Il s’agit de deux logiciels libres, gratuits, téléchargeables en ligne et fonctionnant sous Windows, Mac-OS et Linux.

   
Pour installer R:
    
   * [Installer R sous Windows](https://cloud.r-project.org/bin/windows/base)
    
   * [Installer R sous Mac](https://cloud.r-project.org/bin/macosx)

   * Sous Linux R n’est fourni que comme un outil en ligne de commande, utilisez votre gestionnaire de packages habituel.

    
[RStudio](https://www.rstudio.com/products/RStudio/) est un environnement de développement intégré, qui propose des outils et facilite 
l’écriture de scripts et l’usage de R au quotidien. C’est une interface bien supérieure à celles fournies par défaut lorsqu’on 
installe R sous Windows ou sous Mac-OS
   
   [Installer RStudio](https://www.rstudio.com/products/rstudio/download/#download), téléchargez la version adaptée à votre système :
    
Nous verrons comment installer de nouveaux packages.

## Utilisation de GIT sur votre plate-forme

* La première chose à faire est de l'installer. Voir le site fourni:

[Installation de GIT sur (windows, MacOS,Linux)](https://git-scm.com/book/fr/v1/D%C3%A9marrage-rapide-Installation-de-Git).

* Il existe de nombreux tutoriaux permettant de se former à ce gestionnaire de versions. Voir:

[Tutorial GIT](https://openclassrooms.com/courses/gerez-vos-codes-source-avec-git)  par exemple.
