# Informations pratiques

## Responsable / Intervenante

   Laurence Viry (AIRSEA/LJK - MaiMoSiNE) Laurence.Viry@univ-grenoble-alpes.fr
    
## Lieu de la formation

   Salle de formation, RDC bâtiment IMAG (couloir à droite après les ascenseurs, face à l'accueil)
       Campus universitaire - 700 av. centrale 
   38400 Saint Martin D’Hères.

[Plan d'accès](https://batiment.imag.fr/fr/contact-adresses-plan-dacces)

## Contenu

*   Structure du langage R
*    Fonctionnalités élémentaires du logiciel RStudio
*    Importation et exportation de données contenues dans des fichiers de différents formats (txt, csv)
*   Interfaçage avec logiciel de tableur, focus sur Microsoft Excel
*    Exemple d’analyse exploratoire de données : réaliser des analyses statistiques ou spatiales
*    Exemple de représentation des données : sous forme graphique et cartographique
*    Pour aller plus loin : présentation des possibilités de développement d’outils interactifs avec R

## Mise en oeuvre de la formation

La formation se fera à l'aide:

* de **notebooks R** en utilisant le serveur de notebooks de GRICAD ou l'application jupyter installée sur les postes de travail.<br>

* Du serveur Gitlab de GRICAD pour stocker/récupérer l'ensemble des informations nécessaires à la formation. 

Vous trouverez dans **Consignes.md**, les modalités d'accès à ces serveurs et quelques références à de la documentation.

* Nous travaillerons principalement sur des données fournies par ATMO et quelques exemples simplifiés.
