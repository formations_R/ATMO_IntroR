
Le fichier **poidsTaille.csv** stocké dans le répertoire **data** du projet **CED-IntroR** contient les données d'une 
étude du CHU de Toulouse, concernant le **poids**, la **taille**, le **sexe** et l'**âge** d'enfants de 4 à 7ans.

Ce fichier contient sur sa première ligne le nom des variables observées et sur la première collonne le nom des observations.



```R
poids.Taille <- ....
```

* Quel est la class de l'objet contenant le fichier des données?
* Quelle est la taille de l'échantillon et le nombre de variables observées?
* Quel est le mode et la classe des variables contenues dans cet échantillon?


```R
# classe de l'objet issu de la lecture du fichier de données
class(poids.Taille)
# Taille de l'échantillon

# Nombre de variables
...
# Nom des variables

# Nom des observations

```


```R
# Mode et class de la variable "GENRE"

```


```R
# Mode et class de la variable "Taille"

```


```R
# Mode et class de la variable "Poids"

```


```R
# Mode et class de la variable "AGE"

```

* Calculer les statistques de bases des variables du **data.frame** à l'aide de la fonction **summary**


```R

```

On peut voir rapidement sur les sorties de la fonction *summary* si les variables sont quantitatives ou qualitatives. 

   - Pour les **variables quantitatives**, on calcule le minimum, le 1er quartile, la médiane, la moyenne, le 3 ième quartile et le maximum.
   - Pour les **variables qualitatives**, on obtient le tableau de fréquence.
   
C'est une première validation du mode de traitement des variables.

* Calculer la variable POIDS/TAILLE


```R
PT <- 
```

* On veut traiter les variables **POIDS** et **TAILLE** comme des variables quantitatives, que faut faire?


```R

```

* Calculer séparément *la moyenne, l'étendue, la médiane, les quartiles et la variance* des variables **POIDS** et **TAILLE**.

Si on ne connait pas le nom des fonctions, on utilisera la fonction **help.search** pour les trouver.


```R
help.search("quartiles")
fonctions du package stats
```


```R
....

# Histogramme
n <- 20

```

* Calculer **l'écart-type** des variables **POIDS** et **TAILLE** et écrire la fonction qui calcule l'écart-type sans biais 


```R
sd(poids.Taille$TAILLE) # ecart-type
sd(poids.Taille$POIDS) # ecart-type
std <-function(x,...){

}
std(poids.Taille$TAILLE)
std(poids.Taille$POIDS)
```

* Sauvegarder les données au format R ( RData)


```R

```

* Après avoir initialiser l'espace de travail, charger le fichier que vous venez de sauvegarder avec la commande **load**


```R
# Initialisation de l'esapce de travail
rm(list=ls())

```


```R
ls()

```


```R
boxplot(poids.Taille$POIDS ~ poids.Taille$GENRE, xlab="sexe",)
```

* ** Ecrire un script sous RStudio** ou l'**éditeur de votre choix** effectuant les différentes étapes d travail qui précède.
