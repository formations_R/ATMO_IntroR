## Présentation du cours

Ce cours est une introduction à R et RStudio à destination d'ATMO Auvergne Rhône-Alpes. Il a pour objectif de savoir utiliser des scripts R pour 
analyser des masses de données importantes et en sortir des informations sous forme de tableaux, de graphiques ou de cartographie.  

A l'issue de ce cours, les participants devront être capables de:

- Maîtriser les éléments fondamentaux du langage R.
- Ecrire un code simple.
- Utiliser des procédures statistiques ou de manipulation de données issues de package existants.
- Déchiffrer et utiliser un code plus compliqué (déjà écrit).
- Savoir interfacer R avec Excel.
- Savoir comment représenter les données sous forme graphique et/ou cartographique.

Une **première partie** aborde les raisons du choix de ce logiciel pour le traitement des données, son installation, la documentation,
les packages, les interfaces utilisateurs disponibles (IDE) et une description de RStudio, l'IDE choisit pour ce cours. (IntroR.ipynb,IntroR.Rnw). 
On abordera quelques utilisations de bases.

Dans une **deuxième partie** on présente les concepts de bases : les objets, les fonctions,... (Objet.ipynb, Objet.Rnw).

Dans la **troisième partie**, on aborde les manipulations de données qui sont des opérations courantes dans la pratique statistique: 
importation de données sous différents formats propriétaires, sauvegarde de données et de résultats, traitements des données manquantes, concaténation des niveaux de 
facteurs,... (ManipDon.ipynb, ManipDon.Rnw). On effectuera quelques traitements statistiques sur les données.

Le traitement statistique s'accompagne d'une visualisation claire et synthétique des résultats, nous décrivons dans la **quatrième partie**
les nombreuses possibilités offertes par R dans ce domaine (GraphiqueR.ipynb, GraphiqueR.Rnw).

Enfin dans la **cinquième partie**, nous présentons quelques éléments de programmation, comment construire un script R implémentant
un algorithme statistique ou une chaine de  traitements sur des données. On aborde dans cette partie les capacités de déboguage 
et de profiling de R et rapidement comment utiliser les ressources multi-coeur actuellement disponibles sur les portables ou 
serveurs à notre disposition (ProgrammerR.ipynb, ProgrammerR.Rnw).

Pour chacune de ces parties, vous aurez des fichiers notebook que vous récupérerez sur le serveur GITLAB de GRICAD, un fichier 
[R notebook Markdown](http://rmarkdown.rstudio.com/r_notebooks.html) et une version **pdf*. 

[Pour en savoir plus sur les notebooks (jupyter, Rmarkdown)](https://www.datacamp.com/community/blog/jupyter-notebook-r#gs.6iMBYHw)

### Le serveur GITLAB (GRICAD)
L'ensemble des consignes et des fichiers utiles à ce cours est stocké dans le projet [**CED-IntroR**](https://gricad-gitlab.univ-grenoble-alpes.fr/formations-statistiques-R/CED-IntroR) 
du serveur gitlab de GRICAD, vous avez accès à  ce serveur avec votre <identifiant,mot de passe> AGALAN (voir menu **Consignes**)

### Le serveur de notebooks de l'UMS GRICAD

   * Connectez vous sur [le serveur de notebook de GRICAD](https://jupyterhub.u-ga.fr/) avec votre compte Agalan.\n",
   * Exemple d'utilisation, suivre le cours \"Introduction\" (**introR.ipynb**), première partie de ce cours,(voir menu **Consignes**)

### Installation de R sur votre portable

Pour un meilleur suivi du cours, nous vous conseillons d'installer R (Version 3.4.x au moins) et [RStudio](https://www.rstudio.com/products/RStudio/) 
sur votre ordinateur personnel , de préférence avant la première séance.

Il s’agit de deux logiciels libres, gratuits, téléchargeables en ligne et fonctionnant sous Windows, Mac-OS et Linux.

Pour installer R, il suffit de se rendre sur une des pages suivantes 2 :

   * [Installer R sous Windows](https://cloud.r-project.org/bin/windows/base)

   * [Installer R sous Mac](https://cloud.r-project.org/bin/macosx)

[RStudio](https://www.rstudio.com/products/RStudio/) est un environnement de développement intégré, qui propose des outils et 
facilite l’écriture de scripts et l’usage de R au quotidien. C’est une interface bien supérieure à celles fournies par défaut 
lorsqu’on installe R sous Windows ou sous Mac-OS

[Installer RStudio](https://www.rstudio.com/products/rstudio/download/#download), téléchargez la version adaptée à votre système :

Nous verrons comment installer de nouveaux package.",

### Travailler sur des données personnelles

Nous travaillerons sur des données fournies par ATMO.




